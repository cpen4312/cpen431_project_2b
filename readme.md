CPEN431 - Group 2b
59906157 Duncan Brown
73504152 Johannes Rüschel
65011158 Simon G. Soekilde

ASSIGNMENT A7

The bitbucket repository has a branch called a7_hand_in which we will not modify after deadline.

Running the server
The server is running by default. In case a restart is needed, run:
"java -Xms64m -Xmx64m -jar kvs.jar" in the same directory.

Problems
For a6, we only handed a local test in due to problems with running on planetlab.
Now, we have chosen completely different planetlab nodes, and done the a6 test againg with 20 nodes. 
This result is on the a6 leaderboard.

We do not have planetlab results for a7. We have been refactoring the whole system, making it easier for us to proceed with
replication and healing. We felt this was neccessary to continue. 
The design and implementation of replication and healing has been carried out, but a set of errors
are keeping us from handing in at the moment.

A lot of our time has been spent finding good nodes to use in our testing, so we decided to automize this task. 
Developing a function which constantly checks if all nodes are within good communication standing. 
We then query each node pulling the HashMaps created and merge them to a final (master) list 
which will allow us to ignore the bad nodes and use the good ones.

This idea came from the IsAlive function which we implemented to check whether a node is alive or not. 
These undertakings and refactoring our code are the reasons we are not able to submit a proper A7.

Development (a lot has changed)
Every request arriving at the server is passed to a thread that parses the message, decices which node it is for by hashing it,
and either forwards it or executes in locally. The server that the client contacted is also the server replying back with the response to the client.

The Consistent Hashing is implemented with virtual nodes. That is, a node is hashed multiple times onto the ring.
A node has a hostname appended with a virtual node number, and this indicates the specific virtual node.
If a node fails, all corresponding virtual nodes are removed from the ring.
Initially this scheme ensures load balancing, and when a node fails, load balancing is still ensured.
Adding back nodes is easy, as the same spots will be taken by the node again, ensuring load balancing.

Replication
The scheme we use for replication is key-based. This means that a single key is replicated onto the two successors of the responsible node.
These successors are found as the immediate unique nodes walking clockwise in the hash ring.
This means that keys on the same physical server are replicated at different physical servers.
This scheme relies totally on the key-ring, and every information needed is found by accessing the ring.

Healing (replication after failure)
When a node dies, 3 different data-sets are destroyed: originals, replicas of first predecessor and replicas of second predecessor.
The scheme ensures that the immediate successor of the dead node takes care of re-replicating the original keys onto a new replica.
The predecessors takes care of their own originals (of which the dead node had replicas), and re-replicates them accordingly.
Every successor or predecessor is calculated by using the key-ring, walking the edge and taking virtual nodes into account.

The cache is implemented using a synchronized hash map with a clean up thread running every 1 seconds.

Shutdown verification
In ServerPacketHandler.java line 362 the SHUTDOWN command is processed, issuing the System.exit(0) method.

Additional Error Codes
0x21 - VALUE_LEN_EXCEEDS_BUFFER
0x22 - REQUEST_INCOMPLETE
0x99 - Inexisting command