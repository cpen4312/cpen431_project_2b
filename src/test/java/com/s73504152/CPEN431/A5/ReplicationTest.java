package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.*;

/**
 * Created by Simon on 29-03-2016.
 */
public class ReplicationTest {
    RequestReplyProtocol rrp;
    int port;
    int timeout;
    boolean retry;
    public static final Logger logger = Logger.getLogger(ReplicationTest.class.getName());
    private long sleep = 2000;
    ServerApp[] servers;
    int testCounter = 1;

    public ReplicationTest() {
        setupLogger();

        rrp = new RequestReplyProtocol(logger);
        timeout = 200;
        retry = true;

        servers = new ServerApp[8];
    }

    @Test
    public void test1() {
        try {
            BufferedReader in = new BufferedReader(new FileReader(Constants.SERVER_NODES_TEST));
            boolean localTest = true;
            int localPort;
            boolean debug = true;

            String host = null;
            String line;
            // Start local servers
            int i = 0;
            while ((line = in.readLine()) != null) {

                if (line.length() != 0) {
                    host = line.split(":")[0];
                    InetAddress hostName = InetAddress.getByName(host);
                    localPort = Integer.parseInt(line.split(":")[1]);
                    servers[i] = new ServerApp(hostName, localPort, debug, localTest, false, true);
                    servers[i].runServerApp();
                    i++;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String hostname = "127.0.0.1";
        ERROR error;
        port = 14001;

        int valueLength = 10000;

        byte[] key = ByteArrayHelper.stringToByteArray("0000000000000000000000000000000000000000000000000000000000000001");

        System.out.println("Sending to: " + hostname + ", expects responsible node: " + servers[0].keySpace.getNode(key) + ", h(key): " + servers[0].keySpace.hashFunction(key));

        error = sendMessage(hostname, port, COMMAND.PUT, key, valueLength);
        Assert.assertEquals(ERROR.OK, error);

        key = ByteArrayHelper.stringToByteArray("0000000000000000000000000000000000000000000000000000000000000002");
        error = sendMessage(hostname, port, COMMAND.PUT, key, valueLength);
        Assert.assertEquals(ERROR.OK, error);

        key = ByteArrayHelper.stringToByteArray("0000000000000000000000000000000000000000000000000000000000000003");
        error = sendMessage(hostname, port, COMMAND.PUT, key, valueLength);
        Assert.assertEquals(ERROR.OK, error);

        key = ByteArrayHelper.stringToByteArray("0000000000000000000000000000000000000000000000000000000000000004");
        error = sendMessage(hostname, port, COMMAND.PUT, key, valueLength);
        Assert.assertEquals(ERROR.OK, error);

//        error = sendMessage(hostname, port, COMMAND.GET, key, valueLength);
//        Assert.assertEquals(ResponsePayload.ERROR.OK, error);
//
//        error = sendMessage(hostname, port, COMMAND.REMOVE, key, valueLength);
//        Assert.assertEquals(ERROR.OK, error);
//
//        error = sendMessage(hostname, port, COMMAND.REMOVE, key, valueLength);
//        Assert.assertEquals(ERROR.KEY_NOT_FOUND, error);
//
//        error = sendMessage(hostname, port, COMMAND.GET, key, valueLength);
//        Assert.assertEquals(ERROR.KEY_NOT_FOUND, error);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("KeySpace (from 127.0.0.1)");
        String[] keySpace = servers[0].keySpace.getKeySpaceAsStringArray();
        for (int i = 0; i < keySpace.length; i++) {
            System.out.println("[" + i + "] " + keySpace[i]);
        }

        //printKeyDistribution(servers);

        System.out.println("Successors of node 2 based on key");
        for (String node: servers[1].keySpace.getSuccessors(key, false, false)) {
            System.out.println(node);
        }

        // Shut down server 2
        hostname = "127.0.0.2";

        sendMessage(hostname, port, COMMAND.SHUTDOWN, key, 0);
        // Dont' expect an error as it is immediately shut down


        for (ServerApp server : servers) {
            server.keySpace.removeNode(hostname);
        }

        // inform all nodes that 2 has died
        for (ServerApp server : servers) {
            // dont do it for server 2, since it's down
            if(server.address.getHostAddress() != hostname){
                logger.log(Level.INFO, "deadNode() call on " + server.address.getHostAddress());
                server.replicationManager.deadNode(hostname);
            }
        }

        // Now server 5 is responsible for the key
        System.out.println("Responsible node is now: " + servers[0].keySpace.getNode(key) + ", h(key): " + servers[0].keySpace.hashFunction(key));

        System.out.println("KeySpace (from 127.0.0.1)");
        keySpace = servers[0].keySpace.getKeySpaceAsStringArray();
        for (int i = 0; i < keySpace.length; i++) {
            System.out.println("[" + i + "] " + keySpace[i]);
        }
/*
        System.out.println("Server load:");
        for (ServerApp server : servers) {
            System.out.println(server.address.getHostName() + ": " + server.dataStorage.getSize() + " keys");
        }
*/
        System.out.println("Successors of node 5 based on key");
        for (String node: servers[1].keySpace.getSuccessors(key, false, false)) {
            System.out.println(node);
        }
    }

    private void printKeyDistribution(ServerApp[] servers) {
        System.out.println("Key distribution:");
        for (ServerApp server : servers) {
            System.out.println(server.address.getHostName() + ": " + server.dataStorage.getSize());
        }
    }

    private ERROR sendMessage(String hostname, int port, COMMAND command, byte[] key, int valueLength) {
        Request request = new Request(logger);
        RequestPayload requestPayload = null;
        try {
            request.setDestinationAddress(InetAddress.getByName(hostname));
            request.setDestinationPort(port);

            requestPayload = new RequestPayload(logger);
            requestPayload.setCommand(command);
            requestPayload.setKey(key);
            requestPayload.setValue(MessageHelper.generateRandomValue(valueLength));
            request.setPayload(requestPayload);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (KeyLenMismatchException e) {
            e.printStackTrace();
        } catch (PayloadValueTooLongException e) {
            e.printStackTrace();
        }

        RequestReplyProtocol requestReplyProtocol = new RequestReplyProtocol(logger);
        try {
            logger.log(Level.INFO, "TEST [" + testCounter + "]: Sending message to: " + hostname + ", cmd: " + command);
            Response response = requestReplyProtocol.sendAndReceive(request, timeout, retry);

            ResponsePayload responsePayload = (ResponsePayload) response.getPayload();

            logger.log(Level.INFO, "TEST [" + testCounter + "]: Received message, cmd: " + command.toString() + " [" + valueLength + "], err: " + responsePayload.getErrorCode());

            testCounter++;
            Thread.sleep(1000);
            return responsePayload.getErrorCode();
        } catch (RequestTimeoutException e) {
            logger.log(Level.INFO, "Timeout");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

        testCounter++;
        return null;
    }

    /*
   setup custom Formatter for logging
   */
    public void setupLogger()  {
        logger.setUseParentHandlers(false);
        if (logger.getHandlers().length == 0) {
            try {
                Handler cons = new ConsoleHandler();
                cons.setLevel(Constants.LOGGER_LEVEL);
                cons.setFormatter(new SingleLineFormatter(InetAddress.getLocalHost()));
                logger.addHandler(cons);

                Handler file = new FileHandler("logs/myServer.%u.%g.log", Constants.LOGGER_MAX_SIZE_PER_LOGFILE, Constants.LOGGER_MAX_NR_OF_LOGFILES, true);
                file.setLevel(Constants.LOGGER_LEVEL);
                file.setFormatter(new SingleLineFormatter(InetAddress.getLocalHost()));
                logger.addHandler(file);
            }
            catch (IOException e) {
                System.out.println("ERR: Could not initialize log file.");
            }
        }
    }

}
