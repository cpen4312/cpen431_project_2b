package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.PacketEmptyException;
import com.s73504152.CPEN431.A5.Exceptions.PacketIncompleteException;
import com.s73504152.CPEN431.A5.Exceptions.PayloadIncompleteException;
import com.s73504152.CPEN431.A5.Exceptions.PayloadNoCommandException;
import com.s73504152.CPEN431.A5.Helper.*;
import org.junit.*;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.net.InetAddress;
import java.util.LinkedList;
import java.util.Random;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * Created by Johannes on 12.04.2016.
 */
public class RequestTest {
    Logger logger = Logger.getLogger(RequestPayloadTest.class.getName());
    Request request;

    Random random = new Random();
    private int sourcePort = 55555;

    @Before
    public void setUp() throws Exception {
        this.request = new Request(logger);
    }

    @Test
    public void getPortsTest() throws Exception {
        int sourcePort = 15555;
        int dstPort = 16666;
        request.setSourcePort(sourcePort);
        request.setDestinationPort(dstPort);

        assertEquals(request.getSourcePort(), sourcePort);
        assertEquals(request.getDestinationPort(), dstPort);
    }

    @Test
    public void getPorts_missing_Test() throws Exception {
        assertEquals(-1, request.getSourcePort());
        assertEquals(-1, request.getDestinationPort());
    }

    @Test
    public void getAddressesTest() throws Exception {
        LinkedList<InetAddress> toTest = new LinkedList<InetAddress>();

        toTest.add(InetAddress.getByName("localhost"));
        toTest.add(InetAddress.getByName("127.0.0.1"));
        toTest.add(InetAddress.getByName("169.254.154.20"));
        toTest.add(InetAddress.getByName("www.google.de"));

        for (InetAddress address : toTest) {
            request.setSourceAddress(address);
            request.setDestinationAddress(address);
            Assert.assertArrayEquals(address.getAddress(), request.getDestinationAddress().getAddress());
            Assert.assertArrayEquals(address.getAddress(), request.getSourceAddress().getAddress());
        }

    }


    @Test
    public void getAddresses_null_Test() throws Exception {
        InetAddress address = null;

        request.setSourceAddress(address);
        request.setDestinationAddress(address);
        assertNull(request.getDestinationAddress());
        assertNull(request.getSourceAddress());
    }


    @Test
    public void getMessageIdAsStringTest() throws Exception {
        int sourcePort = 15555;
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);
        String stringMsgId = ByteArrayHelper.byteArrayToString(msgId);

        request.setMessageId(msgId);
        assertEquals(stringMsgId, request.getMessageIdAsString());
    }

    @Test
    public void getMessageIdTest() throws Exception {
        int sourcePort = 15555;
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);

        request.setMessageId(msgId);
        assertArrayEquals(msgId, request.getMessageId());
    }

    @Test
    public void getMessageIdAsString_msgId_null_Test() throws Exception {
        assertNull(request.getMessageId());
    }

    @Test
    public void getPayloadTest() throws Exception {
        COMMAND cmd = COMMAND.DELETE_ALL;
        RequestPayload payload = new RequestPayload(logger);
        payload.setCommand(cmd);

        request.setPayload(payload);

        assertEquals(payload, request.getPayload());
    }

    @Test
    public void parseDataTest() throws Exception {
        byte[] requestBytes;
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);
        COMMAND cmd = COMMAND.DELETE_ALL;

        RequestPayload payload = new RequestPayload(logger);
        payload.setCommand(cmd);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(msgId);
        out.write(payload.getPayloadBytes());
        requestBytes = out.toByteArray();

        request.parseData(requestBytes);
        assertArrayEquals(msgId, request.getMessageId());
    }

    @Test
    public void parseData_null_Test() throws Exception {
        try {
            request.parseData(null);
        }
        catch (PacketEmptyException e) { }
    }

    @Test
    public void parseData_payload_incomplete_Test() throws Exception {
        byte[] responseBytes;
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(msgId);
        responseBytes = out.toByteArray();

        int newLen = 10;
        byte[] corrupted = new byte[newLen];
        System.arraycopy(responseBytes, 0, corrupted, 0, newLen);

        try {
            request.parseData(corrupted);
        }
        catch (PacketIncompleteException e) {}
    }


    @Test
    public void getPacketBytesTest() throws Exception {
        byte[] requestBytes;
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);
        COMMAND cmd = COMMAND.DELETE_ALL;

        RequestPayload payload = new RequestPayload(logger);
        payload.setCommand(cmd);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(msgId);
        out.write(payload.getPayloadBytes());
        requestBytes = out.toByteArray();

        request.setMessageId(msgId);
        request.setPayload(payload);
        assertArrayEquals(requestBytes, request.getPacketBytes());
    }

    @Test
    public void getPacketBytes_MsgId_null_Test() throws Exception {
        byte[] requestBytes;
        COMMAND cmd = COMMAND.DELETE_ALL;

        RequestPayload payload = new RequestPayload(logger);
        payload.setCommand(cmd);

        request.setPayload(payload);
        request.getPacketBytes();
    }

    @Test
    public void getPacketBytes_payload_null_Test() throws Exception {
        try {
            request.getPacketBytes();
        } catch (PayloadIncompleteException e) {
        }
    }

}