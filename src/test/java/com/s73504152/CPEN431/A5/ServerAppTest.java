package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Helper.ByteArrayHelper;
import com.s73504152.CPEN431.A5.Helper.Constants;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import java.net.*;
import java.util.logging.Logger;

import org.junit.Test;

/**
 * Created by Johannes on 17.01.2016.
 */
public class ServerAppTest {

    DatagramSocket clientSocket;
    ServerApp server;

    public static final Logger logger = Logger.getLogger(ServerAppTest.class.getName());

    public ServerAppTest(DatagramSocket clientSocket) {
        this.clientSocket = clientSocket;
        try {
            InetAddress hostName = Inet4Address.getByName("localhost");
            this.server = new ServerApp(hostName, Constants.SERVER_PUBLIC_PORT, true, true, false, true);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();


    @Test
    public void ConnectionTest() {
        String message = "1";
        UdpClient client = new UdpClient(message.getBytes());
    }

    @Test
    public void PutGetSuccess() {

    }

    private class UdpClient {
        DatagramSocket clientSocket;

        private String destinationAddress = "127.0.0.0.1";
        private int destinationPort = 14152;
        public String messageID;
        public Request request;
        public Response response;

        public UdpClient(byte[] message) {
            try (DatagramSocket socket = new DatagramSocket()) {

                InetAddress inetAddress = InetAddress.getByName(destinationAddress);
                Request r = new Request(logger);

                String requestIdString = r.getMessageIdAsString();
                byte[] requestId = r.getMessageId();
                ByteArrayHelper.byteArrayToString(requestId);


            } catch (SocketException e) {
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }


    }
}
