package com.s73504152.CPEN431.A5;

import org.junit.Test;

import java.io.IOException;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * Created by Simon on 04-03-2016.
 */
public class ConsistentHashingTest {
    public static final Logger logger = Logger.getLogger(ConsistentHashingTest.class.getName());

    @Test
    public void testConsistentHashing() {
        ConsistentHashing ch = new ConsistentHashing(null, logger);

        /* Good nodes:
            lim-planetlab-1.univ-reunion.fr
            planetlab2.emich.edu
            iraplab2.iralab.uni-karlsruhe.de
            planetlab5.eecs.umich.edu
         */

        /*
        addNode(0, "planetlab2.cs.ubc.ca");
        addNode(1, "host2.planetlab.informatik.tu-darmstadt.de");
        addNode(2, "lim-planetlab-1.univ-reunion.fr");
        addNode(3, "planetlab2.emich.edu");
        addNode(4, "node5");
        addNode(5, "node6");
        */

        try {
            ch.initNodes("C:\\Users\\Simon\\Documents\\Repositories\\cpen431_project_2b\\CH_serverlist.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Key Space:");

        for (String node : ch.getServerStatusList()) {
            System.out.println("Node: " + node);
        }

        byte[] key1 = "21874612".getBytes();
        byte[] key2 = "872312242151".getBytes();
        byte[] key3 = "222222232".getBytes();
        byte[] key4 = "23332232".getBytes();
        byte[] key5 = "22232".getBytes();

        String node1 = ch.getNode(key1);
        String node2 = ch.getNode(key2);
        String node3 = ch.getNode(key3);
        String node4 = ch.getNode(key4);
        String node5 = ch.getNode(key5);

        System.out.println();
        System.out.println("Key1 is stored at: " +ch.getNode(key1));
        System.out.println("Key2 is stored at: " +ch.getNode(key2));
        System.out.println("Key3 is stored at: " +ch.getNode(key3));
        System.out.println("Key4 is stored at: " +ch.getNode(key4));
        System.out.println("Key5 is stored at: " +ch.getNode(key5));

        System.out.println("\nSay, UBC node goes down...");
        ch.removeNode("planetlab2.cs.ubc.ca");

        System.out.println("Key1 is stored at: " +ch.getNode(key1));
        System.out.println("Key2 is stored at: " +ch.getNode(key2));
        System.out.println("Key3 is stored at: " +ch.getNode(key3));
        System.out.println("Key4 is stored at: " +ch.getNode(key4));
        System.out.println("Key5 is stored at: " +ch.getNode(key5));
        System.out.println("\nKey Space:");

        System.out.println("Key Space:");

        for (String node : ch.getServerStatusList()) {
            System.out.println("Node: " + node);
        }

        System.out.println("\nSay, Darmstadt node goes down...");
        ch.removeNode("host2.planetlab.informatik.tu-darmstadt.de");

        System.out.println("Key1 is stored at: " +ch.getNode(key1));
        System.out.println("Key2 is stored at: " +ch.getNode(key2));
        System.out.println("Key3 is stored at: " +ch.getNode(key3));
        System.out.println("Key4 is stored at: " +ch.getNode(key4));
        System.out.println("Key5 is stored at: " +ch.getNode(key5));

        System.out.println("\nKey Space:");
        System.out.println("Key Space:");

        for (String node : ch.getServerStatusList()) {
            System.out.println("Node: " + node);
        }

        System.out.println("\nSay, Node5 node goes down...");
        ch.removeNode("node5");


        System.out.println("Key1 is stored at: " +ch.getNode(key1));
        System.out.println("Key2 is stored at: " +ch.getNode(key2));
        System.out.println("Key3 is stored at: " +ch.getNode(key3));
        System.out.println("Key4 is stored at: " +ch.getNode(key4));
        System.out.println("Key5 is stored at: " +ch.getNode(key5));

        System.out.println("\nKey Space:");
        System.out.println("Key Space:");

        for (String node : ch.getServerStatusList()) {
            System.out.println("Node: " + node);
        }

        System.out.println("\nSay, Darmstadt is added back...");
        ch.addNode("host2.planetlab.informatik.tu-darmstadt.de");

        System.out.println("Key1 is stored at: " +ch.getNode(key1));
        System.out.println("Key2 is stored at: " +ch.getNode(key2));
        System.out.println("Key3 is stored at: " +ch.getNode(key3));
        System.out.println("Key4 is stored at: " +ch.getNode(key4));
        System.out.println("Key5 is stored at: " +ch.getNode(key5));

        System.out.println("\nKey Space:");
        System.out.println("Key Space:");

        for (String node : ch.getServerStatusList()) {
            System.out.println("Node: " + node);
        }

        /*
        // Check if all current buckets matches
        assertEquals(ch.getNodeList()[0], ch.getAllNodes()[1]);
        assertEquals(ch.getNodeList()[1], ch.getAllNodes()[1]);
        assertEquals(ch.getNodeList()[2], ch.getAllNodes()[2]);
        assertEquals(ch.getNodeList()[3], ch.getAllNodes()[3]);
        assertEquals(ch.getNodeList()[4], ch.getAllNodes()[5]);
        assertEquals(ch.getNodeList()[5], ch.getAllNodes()[5]);
    */
    }

    private static void printByteArray(byte[] buf) {
        for (int i = 0; i < buf.length; i++) {
            System.out.print(buf[i] + " ");
        }
        System.out.println();
    }

}
