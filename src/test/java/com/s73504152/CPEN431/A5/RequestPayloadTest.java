package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.ByteArrayHelper;
import com.s73504152.CPEN431.A5.Helper.COMMAND;
import com.s73504152.CPEN431.A5.Helper.Constants;
import com.s73504152.CPEN431.A5.Helper.MessageHelper;
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Logger;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by Johannes on 10.04.2016.
 */
public class RequestPayloadTest {
    Logger logger = Logger.getLogger(RequestPayloadTest.class.getName());
    RequestPayload requestPayload = new RequestPayload(logger);
    int valueLength = 10;

    String stringValue = ByteArrayHelper.byteArrayToHex(MessageHelper.generateRandomValue(valueLength));
    String stringKey = ByteArrayHelper.byteArrayToHex(MessageHelper.generateRandomKey());

    int sourcePort = 55555;

    Random random = new Random();

    @Before
    public void setUp() throws Exception {

    }


    @Test
    public void getCommandTest() throws Exception {
        requestPayload.setCommand(COMMAND.GET);
        assertEquals(COMMAND.GET, requestPayload.getCommand());

    }

    @Test
    public void getKeyAsStringTest() throws Exception {
        byte[] key = ByteArrayHelper.stringToByteArray(stringKey);
        requestPayload.setKey(key);
        assertEquals(stringKey, requestPayload.getKeyAsString());

    }

    @Test
    public void getKeyTest() throws Exception {
        byte[] key = ByteArrayHelper.stringToByteArray(stringKey);
        requestPayload.setKey(key);
        assertArrayEquals(key, requestPayload.getKey());
    }

    @Test
    public void get_key_missing_Test() {
        COMMAND cmd = COMMAND.GET;

        RequestPayload payload = new RequestPayload(logger);
        payload.setCommand(cmd);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            out.write(payload.getPayloadBytes());
        } catch (PayloadIncompleteException e) {

        } catch (PayloadEmptyException e1) {
            e1.printStackTrace();
        } catch (PayloadValueLengthException e1) {
            e1.printStackTrace();
        } catch (PayloadValueException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    @Test
    public void getValueAsStringTest() throws Exception {
        byte[] value = ByteArrayHelper.stringToByteArray(stringValue);
        requestPayload.setValue(value);
        assertEquals(stringValue, requestPayload.getValueAsString());

    }

    @Test
    public void getValueLengthTest() throws Exception {
        byte[] value = ByteArrayHelper.stringToByteArray(stringValue);
        requestPayload.setValue(value);
        assertEquals(value.length, requestPayload.getValueLength());
    }


    @Test
    public void getValueTest() throws Exception {
        byte[] value = ByteArrayHelper.stringToByteArray(stringValue);
        requestPayload.setValue(value);
        assertArrayEquals(value, requestPayload.getValue());
    }

    @Test
    public void getPayloadBytes_PUT_Test() throws Exception {
        COMMAND cmd = COMMAND.PUT;

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(ByteArrayHelper.intToByteArray(cmd.getId()));
        out.write(ByteArrayHelper.stringToByteArray(stringKey));
        out.write(ByteArrayHelper.shortToByteArray((short) ((ByteArrayHelper.stringToByteArray(stringValue)).length)));
        out.write(ByteArrayHelper.stringToByteArray(stringValue));
        byte[] expected = out.toByteArray();

        requestPayload.setCommand(COMMAND.PUT);
        requestPayload.setKey(ByteArrayHelper.stringToByteArray(stringKey));
        requestPayload.setValue(ByteArrayHelper.stringToByteArray(stringValue));
        byte[] actual = requestPayload.getPayloadBytes();

        assertArrayEquals(expected, actual);
    }

    @Test
    public void getPayloadBytes_GET_Test() throws Exception {
        COMMAND cmd = COMMAND.GET;

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(ByteArrayHelper.intToByteArray(cmd.getId()));
        out.write(ByteArrayHelper.stringToByteArray(stringKey));
        byte[] expected = out.toByteArray();

        requestPayload.setCommand(COMMAND.GET);
        requestPayload.setKey(ByteArrayHelper.stringToByteArray(stringKey));
        byte[] actual = requestPayload.getPayloadBytes();

        assertArrayEquals(expected, actual);
    }

    @Test
    public void getPayloadBytes_REMOVE_Test() throws Exception {
        COMMAND cmd = COMMAND.REMOVE;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(ByteArrayHelper.intToByteArray(cmd.getId()));
        out.write(ByteArrayHelper.stringToByteArray(stringKey));
        byte[] expected = out.toByteArray();

        requestPayload.setCommand(COMMAND.REMOVE);
        requestPayload.setKey(ByteArrayHelper.stringToByteArray(stringKey));
        requestPayload.setValue(ByteArrayHelper.stringToByteArray(stringValue));
        byte[] actual = requestPayload.getPayloadBytes();

        assertArrayEquals(expected, actual);
    }

    @Test
    public void getPayloadBytes_DELETE_ALL_Test() throws Exception {
        COMMAND cmd = COMMAND.DELETE_ALL;

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(ByteArrayHelper.intToByteArray(cmd.getId()));
        byte[] expected = out.toByteArray();

        requestPayload.setCommand(cmd);
        byte[] actual = requestPayload.getPayloadBytes();

        assertArrayEquals(expected, actual);
    }

    @Test
    public void getPayloadBytes_SHUTDOWN_Test() throws Exception {
        COMMAND cmd = COMMAND.SHUTDOWN;

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(ByteArrayHelper.intToByteArray(cmd.getId()));
        byte[] expected = out.toByteArray();

        requestPayload.setCommand(cmd);
        byte[] actual = requestPayload.getPayloadBytes();

        assertArrayEquals(expected, actual);
    }

    @Test
    public void getPayloadBytes_GET_PID_Test() throws Exception {
        COMMAND cmd = COMMAND.GET_PID;

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(ByteArrayHelper.intToByteArray(cmd.getId()));
        byte[] expected = out.toByteArray();

        requestPayload.setCommand(cmd);
        byte[] actual = requestPayload.getPayloadBytes();

        assertArrayEquals(expected, actual);
    }

    @Test
    public void getPayloadBytes_IS_ALIVE_Test() throws Exception {
        COMMAND cmd = COMMAND.IS_ALIVE;

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(ByteArrayHelper.intToByteArray(cmd.getId()));
        byte[] expected = out.toByteArray();

        requestPayload.setCommand(cmd);
        byte[] actual = requestPayload.getPayloadBytes();

        assertArrayEquals(expected, actual);
    }

    @Test
    public void getPayloadBytes_IS_ALIVE_OLD_Test() throws Exception {
        COMMAND cmd = COMMAND.IS_ALIVE_OLD;

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(ByteArrayHelper.intToByteArray(cmd.getId()));
        byte[] expected = out.toByteArray();

        requestPayload.setCommand(cmd);
        byte[] actual = requestPayload.getPayloadBytes();

        assertArrayEquals(expected, actual);
    }

    @Test
    public void parseByteArray_GET_Test() throws Exception {
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);

        ByteOutputStream out = new ByteOutputStream();
        out.write((byte) COMMAND.GET.getId());
        out.write(ByteArrayHelper.stringToByteArray(stringKey));

        requestPayload.parseByteArray(out.getBytes(), msgId);
        assertEquals(requestPayload.getCommand(), COMMAND.GET);
        assertArrayEquals(requestPayload.getKey(), ByteArrayHelper.stringToByteArray(stringKey));
    }

    @Test
    public void parseByteArray_GET_no_key_Test() throws Exception {
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);

        ByteOutputStream out = new ByteOutputStream();
        out.write((byte) COMMAND.GET.getId());

        requestPayload.parseByteArray(out.getBytes(), msgId);
        assertEquals(requestPayload.getCommand(), COMMAND.GET);
        assertArrayEquals(new byte[Constants.KEY_LEN], requestPayload.getKey());
    }

    @Test
    public void parseByteArrayFromPutTest() throws Exception {
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write((byte) COMMAND.PUT.getId());
        out.write(ByteArrayHelper.stringToByteArray(stringKey));
        out.write(ByteArrayHelper.shortToByteArray((short) valueLength));
        out.write(ByteArrayHelper.stringToByteArray(stringValue));


        requestPayload.parseByteArray(out.toByteArray(), msgId);
        assertEquals(requestPayload.getCommand(), COMMAND.PUT);
        assertArrayEquals(requestPayload.getKey(), ByteArrayHelper.stringToByteArray(stringKey));
        assertArrayEquals(requestPayload.getValue(), ByteArrayHelper.stringToByteArray(stringValue));
    }

    @Test
    public void generateRandomKey() throws Exception {
        byte[] randomKey = MessageHelper.generateRandomKey();
        assertEquals(Constants.KEY_LEN, randomKey.length);
    }

    @Test
    public void generateRandomValue() throws Exception {
        int valLen = 10;
        byte[] randomValue = MessageHelper.generateRandomValue(valLen);
        assertEquals(valLen, randomValue.length);
    }

}