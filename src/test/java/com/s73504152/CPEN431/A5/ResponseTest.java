package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.PacketEmptyException;
import com.s73504152.CPEN431.A5.Exceptions.PacketIncompleteException;
import com.s73504152.CPEN431.A5.Exceptions.PayloadIncompleteException;
import com.s73504152.CPEN431.A5.Exceptions.PayloadNoCommandException;
import com.s73504152.CPEN431.A5.Helper.*;
import org.junit.*;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.instanceOf;

import java.io.ByteArrayOutputStream;
import java.net.InetAddress;
import java.util.LinkedList;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * Created by Johannes on 12.04.2016.
 */
public class ResponseTest {
    Logger logger = Logger.getLogger(RequestPayloadTest.class.getName());
    Response response;

    int valueLength = 10;
    String stringValue = ByteArrayHelper.byteArrayToHex(MessageHelper.generateRandomValue(valueLength));

    int sourcePort = 55555;

    @Before
    public void setup() {
        this.response = new Response(logger);
    }

    @Test
    public void getMessageIdAsStringTest() throws Exception {
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);
        String stringMsgId = ByteArrayHelper.byteArrayToString(msgId);

        response.setMessageId(msgId);
        assertEquals(stringMsgId, response.getMessageIdAsString());
    }

    @Test
    public void getMessageIdAsString_msgId_null_Test() throws Exception {
        assertEquals("", response.getMessageIdAsString());
    }

    @Test
    public void getAddressesTest() throws Exception {
        LinkedList<InetAddress> toTest = new LinkedList<InetAddress>();

        toTest.add(InetAddress.getByName("localhost"));
        toTest.add(InetAddress.getByName("127.0.0.1"));
        toTest.add(InetAddress.getByName("169.254.154.20"));
        toTest.add(InetAddress.getByName("www.google.de"));

        for (InetAddress address : toTest) {
            response.setSourceAddress(address);
            response.setDestinationAddress(address);
            Assert.assertArrayEquals(address.getAddress(), response.getDestinationAddress().getAddress());
            Assert.assertArrayEquals(address.getAddress(), response.getSourceAddress().getAddress());
        }
    }

    @Test
    public void getAddresses_null_Test() throws Exception {
        InetAddress address = null;

        response.setSourceAddress(address);
        response.setDestinationAddress(address);
        assertNull(response.getDestinationAddress());
        assertNull(response.getSourceAddress());
    }

    @Test
    public void getPortsTest() throws Exception {
        int sourcePort = 15555;
        int dstPort = 16666;
        response.setSourcePort(sourcePort);
        response.setDestinationPort(dstPort);

        assertEquals(response.getSourcePort(), sourcePort);
        assertEquals(response.getDestinationPort(), dstPort);
    }

    @Test
    public void getPorts_missing_Test() throws Exception {
        assertEquals(-1, response.getSourcePort());
        assertEquals(-1, response.getDestinationPort());
    }

    @Test
    public void getMessageIdTest() throws Exception {
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);

        response.setMessageId(msgId);
        assertArrayEquals(msgId, response.getMessageId());
    }

    @Test
    public void getMessageId_msgId_null_Test() throws Exception {
        assertNull(response.getMessageId());
    }

    @Test
    public void getPayloadTest() throws Exception {
        ERROR err = ERROR.OK;
        ResponsePayload payload = new ResponsePayload(logger);
        payload.setErrorCode(err);

        response.setPayload(payload);

        assertEquals(payload, response.getPayload());
    }

    @Test
    public void parseData_DELETE_ALL_Test() throws Exception {
        byte[] responseBytes;
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);
        COMMAND cmd = COMMAND.DELETE_ALL;
        ERROR err = ERROR.OK;

        ResponsePayload payload = new ResponsePayload(logger);
        payload.setErrorCode(err);
        payload.setCommand(cmd);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(msgId);
        out.write(payload.getPayloadBytes());
        responseBytes = out.toByteArray();

        response.parseData(responseBytes);
        assertArrayEquals(msgId, response.getMessageId());
        assertEquals(err, response.getPayload().getErrorCode());

    }

    @Test
    public void parseData_GET_Test() throws Exception {
        byte[] responseBytes;
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);
        COMMAND cmd = COMMAND.GET;
        ERROR err = ERROR.OK;

        ResponsePayload payload = new ResponsePayload(logger);
        payload.setErrorCode(err);
        payload.setCommand(cmd);
        payload.setValue(ByteArrayHelper.stringToByteArray(stringValue));

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(msgId);
        out.write(payload.getPayloadBytes());
        responseBytes = out.toByteArray();

        response.parseData(responseBytes);
        assertArrayEquals(msgId, response.getMessageId());
        assertThat(new ResponsePayload(logger), instanceOf(response.getPayload().getClass()));

    }

    @Test
    public void parseData_payload_incomplete_Test() throws Exception {
        byte[] responseBytes;
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);
        ERROR err = ERROR.OK;
        COMMAND cmd = COMMAND.GET;

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(msgId);
        responseBytes = out.toByteArray();

        int newLen = 10;
        byte[] corrupted = new byte[newLen];
        System.arraycopy(responseBytes, 0, corrupted, 0, newLen);

        try {
            response.parseData(corrupted);
        } catch (PacketIncompleteException e) {
        }
    }


    @Test
    public void parseData_null_Test() throws Exception {
        try {
            response.parseData(null);
        } catch (PacketEmptyException e) {
        }
    }

    @Test
    public void getPacketBytesTest() throws Exception {
        byte[] responseBytes;
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);
        COMMAND cmd = COMMAND.GET;
        ERROR err = ERROR.OK;

        ResponsePayload payload = new ResponsePayload(logger);
        payload.setCommand(cmd);
        payload.setErrorCode(err);
        payload.setValue(ByteArrayHelper.stringToByteArray(stringValue));

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(msgId);
        out.write(payload.getPayloadBytes());
        responseBytes = out.toByteArray();

        response.setMessageId(msgId);
        response.setPayload(payload);
        assertArrayEquals(responseBytes, response.getPacketBytes());

    }

    @Test
    public void getPacketBytes_msgId_null_Test() throws Exception {
        ERROR err = ERROR.OK;
        COMMAND cmd = COMMAND.DELETE_ALL;

        ResponsePayload payload = new ResponsePayload(logger);
        payload.setErrorCode(err);
        payload.setCommand(cmd);

        response.setPayload(payload);
        response.getPacketBytes();

    }

    @Test
    public void getPacketBytes_payload_null_Test() throws Exception {
        try {
            response.getPacketBytes();
        } catch (PayloadIncompleteException e) {
        }

    }
}