package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.KeyLenMismatchException;
import com.s73504152.CPEN431.A5.Exceptions.StorageFullException;
import com.s73504152.CPEN431.A5.Exceptions.KeyNotFoundException;
import com.s73504152.CPEN431.A5.Exceptions.OutOfHeapSpaceException;
import com.s73504152.CPEN431.A5.Helper.ByteArrayHelper;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by Johannes on 14.01.2016.
 */
public class StorageTest {

    public static final Logger logger = Logger.getLogger(ServerApp.class.getName());


    private Storage storage;
    private Map<String, String> demoMap;

    private byte[] keyValidLen = ByteArrayHelper.stringToByteArray("AABBCCDDEEFFAABBCCDDEEFFAABBCCDDAABBCCDDEEFFAABBCCDDEEFFAABBCCDD");
    private byte[] keyInvalidLen = ByteArrayHelper.stringToByteArray("12");
    private byte[] valueDefault = ByteArrayHelper.stringToByteArray("BB");

    public StorageTest() {
        this.storage = new Storage(logger);
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void StorageAvailableTest() {

        Assert.assertNotNull(this.storage.getStorageObject());
    }

    @Test
    public void StorageGetValueTest() {

        try {
            this.storage.put(this.keyValidLen, this.valueDefault);
            byte[] retrievedValue = this.storage.get(this.keyValidLen);
            Assert.assertEquals(retrievedValue, this.valueDefault);
        }
        catch (KeyLenMismatchException e) {
            Assert.fail();
        }
        catch (KeyNotFoundException e) {
            Assert.fail();
        } catch (OutOfHeapSpaceException e) {
            Assert.fail();
        } catch (StorageFullException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void StoragePutKeyTest() {

        try{
            this.storage.put(this.keyValidLen, this.valueDefault);
        }
        catch (KeyLenMismatchException e)
        {
            Assert.fail("Key length does not match specification.");
        } catch (OutOfHeapSpaceException e) {
            e.printStackTrace();
        } catch (StorageFullException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void StoragePutKeyInvalidLenTest() throws KeyLenMismatchException {

        try {
            this.storage.put(this.keyInvalidLen, this.valueDefault);
        }
        catch (KeyLenMismatchException e) {

        } catch (OutOfHeapSpaceException e) {
            e.printStackTrace();
        } catch (StorageFullException e) {
            e.printStackTrace();
        }


    }

    @Test
    public void StorageOverwriteExistingKeyTest() {
        try {
            byte[] secondValue = ByteArrayHelper.stringToByteArray("21");
            this.storage.put(this.keyValidLen, this.valueDefault);

            // try again
            this.storage.put(this.keyValidLen, secondValue);
            byte[] retrieved = this.storage.get(this.keyValidLen);
            Assert.assertTrue(secondValue.equals(retrieved));
        }
        catch (KeyLenMismatchException e)
        {
            Assert.fail();
        }
        catch (KeyNotFoundException e) {
            Assert.fail();
        } catch (OutOfHeapSpaceException e) {
            e.printStackTrace();
        } catch (StorageFullException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void StorageRemoveNotExistingKeyTest() {
        try {
            this.storage.put(this.keyValidLen, valueDefault);
            this.storage.remove(this.keyValidLen);
            byte[] value = this.storage.get(keyValidLen);
            exception.expect(KeyNotFoundException.class);
        }
        catch (Exception e) {

        }
    }

}
