package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.ByteArrayHelper;
import com.s73504152.CPEN431.A5.Helper.COMMAND;
import com.s73504152.CPEN431.A5.Helper.Constants;
import com.s73504152.CPEN431.A5.Helper.ERROR;
import org.github.jamm.MemoryMeter;
import org.junit.*;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.net.*;
import java.util.logging.Logger;

import static com.s73504152.CPEN431.A5.Helper.ERROR.OK;

/**
 * Created by Johannes on 17.01.2016.
 */
public class ServerAppPacketHandlerTest {
    public static final Logger logger = Logger.getLogger(ServerAppPacketHandlerTest.class.getName());
    public final int serverPort = Constants.SERVER_PUBLIC_PORT;
    private ServerApp serverApp;
    private ReplicationManager replicationManager;
    public InetAddress serverAddress;
    public boolean externalListener = true;
    public ConsistentHashing serverKeySpace;

    private Storage dataStorage;
    private Cache log;

    @Before
    public void setUp() throws SocketException {
       try {
           this.serverAddress  = InetAddress.getByName("127.0.0.1");
           this.serverApp = new ServerApp(serverAddress, Constants.SERVER_PUBLIC_PORT, false, true, false, true);
           this.serverApp.runServerApp();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        this.log = new Cache(Constants.CACHE_TIME_TO_LIVE, Constants.CACHE_CLEANUP_CYCLE, logger);
        DatagramSocket socket = new DatagramSocket();
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void GetPIDTest() {
        try {
            RequestPayload p = new RequestPayload(logger);
            p.setCommand(COMMAND.GET_PID);
            Request r = new Request(logger);
            r.setPayload(p);
            r.setDestinationAddress(serverAddress);
            r.setDestinationPort(Constants.SERVER_PUBLIC_PORT);

            RequestReplyProtocol sender = new RequestReplyProtocol(logger);
            sender.sendAndReceive(r, Constants.SERVER_RECEIVE_TIMEOUT, true);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void EmptyPayloadThrowsExceptionTest() {
        byte[] requestId = ByteArrayHelper.stringToByteArray("A9FE52C70A007F3200000152518DDD5A");
        DatagramPacket packet = new DatagramPacket(requestId, requestId.length);

        try {
            // meter not implemented yet
            MemoryMeter meter = new MemoryMeter();
            ServerPacketHandler handler = new ServerPacketHandler(packet, serverAddress, this.serverPort, true, this.dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
            handler.localExecute();
        } catch (PayloadEmptyException e) {
            e.getStackTrace();
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void GetWithoutKeyThrowsException() {
        try {
            Request request = new Request(logger);
            RequestPayload payload = new RequestPayload(logger);
            payload.setCommand(COMMAND.GET);
            request.setPayload(payload);

            DatagramPacket packet = new DatagramPacket(request.getPacketBytes(), request.getPacketBytes().length);
            // meter not implemented yet
            MemoryMeter meter = new MemoryMeter();
            ServerPacketHandler handler = new ServerPacketHandler(packet, this.serverAddress, this.serverPort, externalListener, this.dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
            handler.localExecute();
        } catch (PayloadEmptyException e1) {
            // e1.printStackTrace();
        } catch (Exception e1) {
            // e1.printStackTrace();
        }
    }

    @Test
    public void GetFailsOnEmptyStorage() {
        byte[] key = new byte[Constants.KEY_LEN];
        String demo_key = "AABBCCDDEEFFAABBCCDDEEFFAABBCCDD";
        key = ByteArrayHelper.stringToByteArray(demo_key);

        try {
            Request request = new Request(logger);
            RequestPayload preparedPayload = new RequestPayload(logger);
            preparedPayload.setCommand(COMMAND.GET);
            preparedPayload.setKey(key);
            request.setPayload(preparedPayload);

            DatagramPacket packet = new DatagramPacket(request.getPacketBytes(), request.getPacketBytes().length);
            MemoryMeter meter = new MemoryMeter();
            ServerPacketHandler handler = new ServerPacketHandler(packet, this.serverAddress, this.serverPort, externalListener, dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
            handler.localExecute();
        } catch (PayloadEmptyException e1) {

        } catch (Exception e1) {

        }
    }

    @Test
    public void RemoveAllThrowsStorageEmptyExceptionOnEmptyStorage() {

        try {
            // REMOVE_ALL
            RequestPayload preparedPayload = new RequestPayload(logger);
            preparedPayload.setCommand(COMMAND.DELETE_ALL);

            Request request = new Request(logger);
            request.setPayload(preparedPayload);

            byte[] rawData = request.getPacketBytes();
            int rawDataLength = rawData.length;

            DatagramPacket packet = new DatagramPacket(rawData, rawDataLength);
            MemoryMeter meter = new MemoryMeter();
            ServerPacketHandler handler = new ServerPacketHandler(packet, this.serverAddress, this.serverPort, externalListener, dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
            new Thread(handler).start();
            // handler.localExecute();
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void RemoveAllSuccess() {
        String demo_key = "AABBCCDDEEFFAABBCCDDEEFFAABBCCDDAABBCCDDEEFFAABBCCDDEEFFAABBCCDD";
        byte[] key = ByteArrayHelper.stringToByteArray(demo_key);
        String demo_key2 = "AABBCCDDEEFFAABBCCDDEEFFAABBCCDDAABBCCDDEEFFAABBCCDDEEFFAABBCCDD";
        byte[] key2 = ByteArrayHelper.stringToByteArray(demo_key);
        byte[] val = ByteArrayHelper.stringToByteArray("12");

        MemoryMeter meter = new MemoryMeter();
        try {
            // PUT first key
            Request request = new Request(logger);
            RequestPayload preparedPayload = new RequestPayload(logger);
            preparedPayload.setCommand(COMMAND.PUT);
            preparedPayload.setKey(key);
            preparedPayload.setValue(val);
            request.setPayload(preparedPayload);

            byte[] rawData = request.getPacketBytes();
            int rawDataLength = rawData.length;

            DatagramPacket packet = new DatagramPacket(rawData, rawDataLength);
            ServerPacketHandler handler = new ServerPacketHandler(packet, this.serverAddress, this.serverPort, externalListener, dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
            new Thread(handler).start();

            // PUT second key
            request = new Request(logger);
            preparedPayload = new RequestPayload(logger);
            preparedPayload.setCommand(COMMAND.PUT);
            preparedPayload.setKey(key2);
            preparedPayload.setValue(val);
            request.setPayload(preparedPayload);

            rawData = request.getPacketBytes();
            rawDataLength = rawData.length;

            packet = new DatagramPacket(rawData, rawDataLength);
            handler = new ServerPacketHandler(packet, this.serverAddress, this.serverPort, externalListener, dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
            new Thread(handler).start();
        } catch (Exception e) {
            Assert.fail();
        }

        try {
            // REMOVE_ALL
            RequestPayload preparedPayload = new RequestPayload(logger);
            preparedPayload.setCommand(COMMAND.DELETE_ALL);
            Request request = new Request(logger);
            request.setPayload(preparedPayload);

            byte[] rawData = request.getPacketBytes();
            int rawDataLength = rawData.length;

            DatagramPacket packet = new DatagramPacket(rawData, rawDataLength);
            ServerPacketHandler handler = new ServerPacketHandler(packet, this.serverAddress, this.serverPort, externalListener, dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
            new Thread(handler).start();

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }

    }

    @Test
    public void PutSuccess() {
        String demo_key = "AABBCCDDEEFFAABBCCDDEEFFAABBCCDD";
        byte[] key = ByteArrayHelper.stringToByteArray(demo_key);
        ERROR expectedError = OK;

        byte[] value = ByteArrayHelper.stringToByteArray("11223344556677889900");

        try {
            Request request = new Request(logger);
            RequestPayload preparedPayload = new RequestPayload(logger);
            preparedPayload.setCommand(COMMAND.PUT);
            preparedPayload.setKey(key);
            preparedPayload.setValue(value);
            request.setPayload(preparedPayload);

            DatagramPacket packet = new DatagramPacket(request.getPacketBytes(), request.getPacketBytes().length);
            MemoryMeter meter = new MemoryMeter();
            ServerPacketHandler handler = new ServerPacketHandler(packet, this.serverAddress, this.serverPort, externalListener, dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
            handler.localExecute();
        } catch (PayloadEmptyException e1) {

        } catch (Exception e1) {

        }
    }

    @Test
    public void PutGetRemoveGetReturnsKeyNotFoundException() throws OutOfHeapSpaceException {
        String demo_key = "AABBCCDDEEFFAABBCCDDEEFFAABBCCDDAABBCCDDEEFFAABBCCDDEEFFAABBCCDD";
        byte[] key = ByteArrayHelper.stringToByteArray(demo_key);

        byte[] valueSent = ByteArrayHelper.stringToByteArray("33223344556677889900");
        byte[] valueRetrieved = null;
        int rawDataLength;
        DatagramPacket packet;
        ServerPacketHandler handler;

        MemoryMeter meter = new MemoryMeter();
        try {
            // prepare payload
            RequestPayload preparedPayload = new RequestPayload(logger);
            preparedPayload.setCommand(COMMAND.PUT);
            preparedPayload.setKey(key);
            preparedPayload.setValue(valueSent);

            // sendAndWaitForResponse packet
            Request request = new Request(logger);
            request.setPayload(preparedPayload);

            byte[] rawData = request.getPacketBytes();
            rawDataLength = rawData.length;

            packet = new DatagramPacket(rawData, rawDataLength);
            handler = new ServerPacketHandler(packet, this.serverAddress, this.serverPort, externalListener, dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
            new Thread(handler).start();

            // get getValue
            RequestPayload preparedPayload2 = new RequestPayload(logger);
            preparedPayload2.setCommand(COMMAND.GET);
            preparedPayload2.setKey(key);

            Request request2 = new Request(logger);
            request2.setPayload(preparedPayload2);

            byte[] rawData2 = request2.getPacketBytes();
            rawDataLength = rawData2.length;

            packet = new DatagramPacket(rawData2, rawDataLength);
            handler = new ServerPacketHandler(packet, this.serverAddress, this.serverPort, externalListener, dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
            new Thread(handler).start();

            // remove key
            RequestPayload removePayload = new RequestPayload(logger);
            removePayload.setCommand(COMMAND.REMOVE);
            removePayload.setKey(key);

            Request removeRequest = new Request(logger);
            removeRequest.setPayload(removePayload);

            byte[] rawRemove = removeRequest.getPacketBytes();

            packet = new DatagramPacket(rawRemove, rawRemove.length);
            handler = new ServerPacketHandler(packet, this.serverAddress, this.serverPort, externalListener, dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
            new Thread(handler).start();
        } catch (KeyLenMismatchException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (PayloadValueTooLongException e1) {
            e1.printStackTrace();
        } catch (PayloadEmptyException e1) {
            e1.printStackTrace();
        } catch (PayloadValueLengthException e) {
            e.printStackTrace();
        } catch (PayloadValueException e) {
            e.printStackTrace();
        } catch (PayloadIncompleteException e) {
            e.printStackTrace();
        }

        try

    {
        // get again
        RequestPayload getPayload = new RequestPayload(logger);
        getPayload.setCommand(COMMAND.GET);
        getPayload.setKey(key);

        Request getReqeust = new Request(logger);
        getReqeust.setPayload(getPayload);

        byte[] rawGet = getReqeust.getPacketBytes();

        packet = new DatagramPacket(rawGet, rawGet.length);
        handler = new ServerPacketHandler(packet, this.serverAddress, this.serverPort, externalListener, dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
        new Thread(handler).start();
    }

    catch(
    Exception e
    )

    {
        Assert.fail();
    }

}

    @Test
    public void PutEqualsGetSuccess() {
        byte[] key = new byte[Constants.KEY_LEN];
        String demo_key = "AABBCCDDEEFFAABBCCDDEEFFAABBCCDDAABBCCDDEEFFAABBCCDDEEFFAABBCCDD";
        key = ByteArrayHelper.stringToByteArray(demo_key);

        byte[] valueSent = ByteArrayHelper.stringToByteArray("33223344556677889900");
        byte[] valueRetrieved = null;
        int rawDataLength;
        DatagramPacket packet;
        ServerPacketHandler handler;

        try {
            // prepare payload
            RequestPayload preparedPayload = new RequestPayload(logger);
            preparedPayload.setCommand(COMMAND.PUT);
            preparedPayload.setKey(key);
            preparedPayload.setValue(valueSent);

            // sendAndWaitForResponse packet
            Request request = new Request(logger);
            request.setPayload(preparedPayload);

            byte[] rawData = request.getPacketBytes();
            rawDataLength = rawData.length;

            packet = new DatagramPacket(rawData, rawDataLength);
            MemoryMeter meter = new MemoryMeter();
            handler = new ServerPacketHandler(packet, this.serverAddress, this.serverPort, externalListener, dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
            new Thread(handler).start();

            // receive answer
            RequestPayload preparedPayload2 = new RequestPayload(logger);
            preparedPayload2.setCommand(COMMAND.GET);
            preparedPayload2.setKey(key);

            Request request2 = new Request(logger);
            request2.setPayload(preparedPayload2);

            byte[] rawData2 = request2.getPacketBytes();
            rawDataLength = rawData2.length;

            packet = new DatagramPacket(rawData2, rawDataLength);
            handler = new ServerPacketHandler(packet, this.serverAddress, this.serverPort, externalListener, dataStorage, log, serverKeySpace, replicationManager, ServerApp.logger, serverApp);
            new Thread(handler).start();

        } catch (Exception e) {
            Assert.fail();
        }
    }
}
