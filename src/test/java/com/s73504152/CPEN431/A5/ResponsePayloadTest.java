package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.ByteArrayHelper;
import com.s73504152.CPEN431.A5.Helper.COMMAND;
import com.s73504152.CPEN431.A5.Helper.ERROR;
import com.s73504152.CPEN431.A5.Helper.MessageHelper;
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import org.junit.*;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.util.Random;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * Created by Johannes on 10.04.2016.
 */
public class ResponsePayloadTest {
    Logger logger = Logger.getLogger(RequestPayloadTest.class.getName());
    ResponsePayload responsePayload = new ResponsePayload(logger);
    int valueLength = 10;
    int sourcePort = 55555;

    String stringValue = ByteArrayHelper.byteArrayToHex(MessageHelper.generateRandomValue(valueLength));
    String stringKey = ByteArrayHelper.byteArrayToHex(MessageHelper.generateRandomKey());

    Random random = new Random();

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void getErrorCode_Test() throws Exception {
        ERROR err = ERROR.OK;
        responsePayload.setErrorCode(err);

        assertEquals(err, responsePayload.getErrorCode());
    }

    @Test
    public void getErrorCode_null_Test() throws Exception {
        assertNull(responsePayload.getErrorCode());
    }

    @Test
    public void getCommand_Test() throws Exception {
        COMMAND cmd = COMMAND.PUT;
        responsePayload.setCommand(cmd);

        assertEquals(cmd, responsePayload.getCommand());
    }

    @Test
    public void getCommand_null_Test() throws Exception {
        assertNull(responsePayload.getCommand());
    }

    @Test
    public void getValue_Test() throws Exception {
        byte[] val = ByteArrayHelper.stringToByteArray(stringValue);
        responsePayload.setValue(val);

        assertArrayEquals(val, responsePayload.getValue());
    }

    @Test
    public void getValue_null_Test() throws Exception {
        assertNull(responsePayload.getValue());
    }

    @Test
    public void getValueAsString_Test() throws Exception {
        byte[] val = ByteArrayHelper.stringToByteArray(stringValue);
        responsePayload.setValue(val);

        assertEquals(stringValue, responsePayload.getValueAsString());
    }

    @Test
    public void getValueAsString_null_Test() throws Exception {
        assertEquals("", responsePayload.getValueAsString());
    }

    @Test
    public void parseByteArray_GET_OK_Test() throws Exception {
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);
        ERROR err = ERROR.OK;

        ByteOutputStream out = new ByteOutputStream();
        out.write((byte) err.getId());
        out.write(ByteArrayHelper.shortToByteArray((short) valueLength));
        out.write(ByteArrayHelper.stringToByteArray(stringValue));

        responsePayload.parseByteArray(out.getBytes(), msgId);
        assertEquals(responsePayload.getErrorCode(), err);
        assertArrayEquals(responsePayload.getValue(), ByteArrayHelper.stringToByteArray(stringValue));
    }

    @Test
    public void parseByteArray_KEYNOTFOUND_Test() throws Exception {
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);
        ERROR err = ERROR.KEY_NOT_FOUND;

        ByteOutputStream out = new ByteOutputStream();
        out.write((byte) err.getId());

        responsePayload.parseByteArray(out.getBytes(), msgId);
        assertEquals(responsePayload.getErrorCode(), err);
    }


    @Test(expected = PayloadEmptyException.class)
    public void parseByteArray_null_Test() throws Exception {
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);

        byte[] payload = null;

        responsePayload.parseByteArray(payload,msgId);

    }

    @Test
    public void parseByteArray_GET_incomplete_Test() throws Exception {
        byte[] msgId = MessageHelper.generateMessageId(sourcePort);
        ERROR err = ERROR.OK;

        ByteOutputStream out = new ByteOutputStream();
        out.write((byte) err.getId());
        out.write(ByteArrayHelper.stringToByteArray(stringValue));

        try{
            responsePayload.parseByteArray(out.getBytes(), msgId);
            Assert.fail();
        } catch (PayloadIncompleteException | PayloadValueLengthExceedsBuffer e) {
        }
    }

    @Test
    public void getPayloadBytes_GET_OK_Test() throws Exception {
        COMMAND cmd = COMMAND.GET;
        ERROR err = ERROR.OK;
        byte[] val = ByteArrayHelper.stringToByteArray(stringValue);
        byte[] valLen = ByteArrayHelper.shortToByteArray((short) valueLength);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write((byte) err.getId());
        out.write(valLen);
        out.write(val);
        byte[] expected = out.toByteArray();

        responsePayload.setCommand(cmd);
        responsePayload.setErrorCode(err);
        responsePayload.setValue(val);

        assertArrayEquals(expected, responsePayload.getPayloadBytes());
    }

    @Test(expected = PayloadIncompleteException.class)
    public void getPayloadBytes_null_Test() throws Exception {
        responsePayload.getPayloadBytes();

    }

    @Test(expected = PayloadIncompleteException.class)
    public void getPayloadBytes_incomplete_Test() throws Exception {
        ERROR err = ERROR.OK;
        byte[] val = ByteArrayHelper.stringToByteArray(stringValue);

        responsePayload.setErrorCode(err);
        responsePayload.setValue(val);
        responsePayload.getPayloadBytes();

    }

}