import com.s73504152.CPEN431.A5.Cache;
import com.s73504152.CPEN431.A5.CachedObject;
import com.s73504152.CPEN431.A5.Helper.Constants;
import com.s73504152.CPEN431.A5.ResponsePayload;
import com.s73504152.CPEN431.A5.ServerApp;

import java.util.logging.Logger;

/**
 * Created by Johannes on 27.01.2016.
 */
public class CacheTest {
    public static final Logger logger = Logger.getLogger(ServerApp.class.getName());

    public static void main(String[] args) {

        Cache cache = new Cache(Constants.CACHE_TIME_TO_LIVE, Constants.CACHE_CLEANUP_CYCLE, logger);

        String key1 = "ABABABABAB";
        String key2 = "CDCDCDCDCD";
        String key3 = "ABABABABAB";

        CachedObject o = new CachedObject(new ResponsePayload(logger));

        cache.put(key1, o);
        cache.printKeys();
        cache.put(key2, o);
        cache.printKeys();
        cache.put(key3, o);
        cache.printKeys();


    }
}
