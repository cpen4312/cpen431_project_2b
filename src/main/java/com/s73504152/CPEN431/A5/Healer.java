package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.COMMAND;
import com.s73504152.CPEN431.A5.Helper.Constants;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Simon on 06-04-2016.
 */
public class Healer implements Runnable {
    ServerApp serverApp;
    Logger logger;
    String targetNode;
    LinkedList<ByteBuffer> keysToForward;

    public Healer(ServerApp serverApp, Logger logger, String targetNode, LinkedList<ByteBuffer> keysToForward) {
        this.serverApp = serverApp;
        this.logger = logger;
        this.targetNode = targetNode;
        this.keysToForward = keysToForward;
    }

    @Override
    public void run() {
        RequestReplyProtocol rrp = new RequestReplyProtocol(logger);

        String me = serverApp.address.getHostAddress(); // for debug

        for (ByteBuffer key : keysToForward) {

            RequestPayload requestPayload = null;
            try {
                requestPayload = new RequestPayload(logger);

                // put key and value into request payload
                requestPayload.setKey(key.array());
                requestPayload.setValue(serverApp.dataStorage.get(key.array()));
                requestPayload.setCommand(COMMAND.PUT_REPLICA);


                // create request
                Request toForward = new Request(logger);
                toForward.setDestinationAddress(InetAddress.getByName(targetNode));
                toForward.setDestinationPort(serverApp.keySpace.getPort(targetNode) + Constants.SERVER_INTERNAL_PORT_OFFSET);
                toForward.setPayload(requestPayload);

                // send request to targetNode
                Response internalResponse = rrp.sendAndReceive(toForward, Constants.SERVER_RECEIVE_TIMEOUT, true);

                Response response = new Response(logger);
                response.getPayload().setCommand(requestPayload.getCommand());

                logger.log(Level.INFO, "[" + me + "] heal: Healing key. Got error code=" + internalResponse.getPayload().getErrorCode() +  " from " + targetNode);


            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (KeyLenMismatchException e) {
                e.printStackTrace();
            } catch (PayloadValueTooLongException e) {
                e.printStackTrace();
            } catch (KeyNotFoundException e) {
                e.printStackTrace();
            } catch (PayloadCommandMissingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (RequestTimeoutException e) {
                e.printStackTrace();
            }


        }

    }
}
