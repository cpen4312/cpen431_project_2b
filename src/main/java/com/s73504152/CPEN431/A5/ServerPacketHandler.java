package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.logging.*;

import static com.s73504152.CPEN431.A5.Helper.ERROR.*;

/**
 * Created by chbndrhnns on 27.01.2016.
 */

public class ServerPacketHandler implements Runnable {
    private final ServerApp serverApp;
    private final ReplicationManager replicationManager;
    private final ConsistentHashing serverKeySpace;
    private final Logger logger;

    private final int serverPort;
    private final InetAddress serverAddress;

    private final Storage dataStorage;
    private final boolean externalListener;
    private final RequestReplyProtocol requestReplyProtocol;

    private final DatagramPacket requestPacket;
    private final Request request;
    private final Response response;

    private final Cache cache;
    private String responsibleNode = "";

    /**
     * Constructor
     * @param packet DatagramPacket
     * @param serverAddress InetAddress
     * @param serverPort port of the listener that contributes to this instance of ServerPacketHandler
     * @param externalListener is it the external listener?
     * @param storage instance of KVS
     * @param cache instance of the cache
     * @param serverKeySpace instance of the ConstistentHashing class
     * @param replicationManager instance of the replicationManager
     * @param logger instance of the logger
     * @param serverApp instance of serverApp
     * @throws PayloadEmptyException
     */
    public ServerPacketHandler(DatagramPacket packet, InetAddress serverAddress, int serverPort, boolean externalListener, Storage storage, Cache cache, ConsistentHashing serverKeySpace, ReplicationManager replicationManager, Logger logger, ServerApp serverApp) throws PayloadEmptyException {
        this.serverApp = serverApp;
        this.logger = logger;
        this.serverAddress = serverAddress;
        this.serverPort = serverPort;
        this.externalListener = externalListener;

        this.dataStorage = storage;
        this.cache = cache;
        this.replicationManager = replicationManager;
        this.serverKeySpace = serverKeySpace;

        // prepare request objects
        this.requestPacket = packet;
        this.request = new Request(logger);

        // prepare response objects
        this.response = new Response(logger);
        this.response.setDestinationAddress(this.requestPacket.getAddress());
        this.response.setDestinationPort(this.requestPacket.getPort());

        int packetLength = packet.getLength();
        int payloadLength = packetLength - Constants.MSG_ID_LEN;

        this.requestReplyProtocol = new RequestReplyProtocol(logger);

        if (payloadLength < Constants.PAYLOAD_MIN_LEN) throw new PayloadEmptyException();
    }

    /**
     * execute the command that was supplied in an incoming packet
     * @throws PayloadCommandMissingException
     */
    public void localExecute() throws PayloadCommandMissingException {
        RequestPayload requestPayload = this.request.getPayload();
        ResponsePayload responsePayload = this.response.getPayload();

        COMMAND cmd;

        try {
            tryGetResponsePayloadFromCache();

            // save command in the response object
            cmd = requestPayload.getCommand();
            responsePayload.setCommand(cmd);
        } catch (PacketRetryException e) {
            return;
        }

        // debug prints
        StringBuilder sb = new StringBuilder();
        if (ServerApp.debug) {
            sb.append("[E] msgId=");
            sb.append(request.getMessageIdAsString());
            sb.append(", cmd=");
            sb.append(responsePayload.getCommand());
        }

        // only do the caching if the switch command was successful
        boolean executeSuccessful = false;

        switch (cmd) {
            case PUT:
            case PUT_REPLICA:
                try {
                    dataStorage.put(requestPayload.getKey(), requestPayload.getValue());
                    responsePayload.setErrorCode(OK);
                    if (ServerApp.debug) {
                        sb.append(", key=");
                        sb.append(requestPayload.getKeyAsString());
                        sb.append(", val=");
                        String value = requestPayload.getValueAsString();
                        sb.append(value.substring(0, Math.min(value.length(), Constants.LOGGER_PRINT_MAX_VAL_LEN)));
                        sb.append("[...]");
                    }

                    executeSuccessful = true;
                } catch (OutOfHeapSpaceException e) {
                    logger.log(Level.INFO, "PUT failed because not enough memory is left. Number of elements=" + this.dataStorage.getSize());
                    // e.printStackTrace();
                    // dataStorage.remove(this.requestPayload.key);
                    responsePayload.setErrorCode(OUT_OF_MEM);
                } catch (StorageFullException e) {
                    logger.log(Level.INFO, "PUT failed because storage is full. Number of elements=" + this.dataStorage.getSize());
                    responsePayload.setErrorCode(OVERLOAD);
                } catch (Exception e) {
                    logger.log(Level.INFO, "General exception: PUT failed for key=" + requestPayload.getKeyAsString());
                    //e.printStackTrace();
                }
                break;
            case GET:
                try {
                    byte[] valueRetrieved = this.dataStorage.get(requestPayload.getKey());
                    responsePayload.setValue(valueRetrieved);
                    responsePayload.setErrorCode(OK);
                    if (ServerApp.debug) {
                        sb.append(", key=");
                        sb.append(requestPayload.getKeyAsString());
                    }
                    executeSuccessful = true;
                } catch (KeyNotFoundException e) {
                    responsePayload.setErrorCode(KEY_NOT_FOUND);
                } catch (Exception e) {
                    logger.log(Level.INFO, "General exception: GET failed for key=" + requestPayload.getKeyAsString());
                    e.printStackTrace();
                }
                break;
            case REMOVE:
            case REMOVE_REPLICA:
                try {
                    dataStorage.remove(requestPayload.getKey());
                    responsePayload.setErrorCode(OK);
                    if (ServerApp.debug) {
                        sb.append(", key=");
                        sb.append(requestPayload.getKeyAsString());
                    }
                    executeSuccessful = true;
                } catch (KeyNotFoundException e) {
                    responsePayload.setErrorCode(KEY_NOT_FOUND);
                } catch (Exception e) {
                    logger.log(Level.INFO, "General exception: REMOVE failed for key=" + requestPayload.getKeyAsString());
                    e.printStackTrace();
                }
                break;
            case SHUTDOWN:
                // nothing happens on shutdown
                break;
            case DELETE_ALL:
                // logger.log(Level.INFO, "nr before purge=" + String.valueOf(dataStorage.getSize()));
                try {
                    dataStorage.removeAll();
                    responsePayload.setErrorCode(OK);
                    executeSuccessful = true;
                } catch (OutOfHeapSpaceException e) {
                    responsePayload.setErrorCode(OUT_OF_MEM);
                } catch (StorageEmptyException e) {
                    executeSuccessful = true;
                    responsePayload.setErrorCode(OK);
                } catch (Exception e) {
                    logger.log(Level.INFO, "General exception: Command DELETE_ALL failed");
                    e.printStackTrace();
                }
                break;
            case MERGE_LISTS:
                try {
                    HashMap<String, Integer> tempMap = HashMapSerializer.unpackageMap(requestPayload.getValue(), logger);
                    HashMapSerializer.addKeys(tempMap, serverApp.masterList);
                    responsePayload.setErrorCode(OK);
                    //executeSuccessful = true;
                } catch (Exception e) {
                    logger.log(Level.INFO, "General exception: Command MERGE_LISTS failed");
                    e.printStackTrace();
                }
                break;
            case IS_ALIVE:
            case IS_ALIVE_OLD:
                responsePayload.setErrorCode(CMD_INVALID);
                executeSuccessful = true;
                break;
            case GET_PID:
                try {
                    int pid = ServerApp.getPID();
                    byte[] b = ByteArrayHelper.intTo4ByteArray(pid);
                    responsePayload.setValue(b);
                    responsePayload.setErrorCode(OK);
                    executeSuccessful = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case INVALID:
                if (ServerApp.debug) {
                    sb.append(", command in request:").append(requestPayload.getCmdByteAsHex());
                    logger.log(Level.INFO, sb.toString());
                }
                responsePayload.setErrorCode(CMD_INVALID);
            default:
                responsePayload.setErrorCode(CMD_INVALID);
                break;
        }

        if (ServerApp.debug) {
//            logger.log(Level.INFO, sb.toString());
        }

        // create a cached object, set the property in the cache map to "processed" and add it to the cache
        if (executeSuccessful) {
            CachedObject cachedObject = new CachedObject(responsePayload);
            cachedObject.setProcessed(true);
            cache.put(request.getMessageIdAsString(), cachedObject);
        } else {
            // if (sb.length() > 0) logger.log(Level.INFO, sb.toString());
        }
        replicateOperation(requestPayload);
    }

    /**
     * try to forward a payload to the responsible node
     * @param requestPayload
     * @return
     */
    private boolean tryForward(RequestPayload requestPayload) {
        boolean success = false;

        //try execute remotely
        if (ServerApp.debug) {
            logger.log(Level.INFO, "[R] for " + responsibleNode + ":" + (serverKeySpace.getPort(responsibleNode) + Constants.SERVER_INTERNAL_PORT_OFFSET));
        }

        Request toForward = new Request(logger);
        try {
            // forward message to other server
            toForward.setDestinationAddress(InetAddress.getByName(responsibleNode));
            toForward.setDestinationPort(serverKeySpace.getPort(responsibleNode) + Constants.SERVER_INTERNAL_PORT_OFFSET);
            toForward.setMessageId(this.request.getMessageId());
            toForward.setPayload(requestPayload);

            // get response and responsePayload
            Response internalResponse = requestReplyProtocol.sendAndReceive(toForward, Constants.SERVER_RECEIVE_TIMEOUT, true);
            // if (ServerApp.debug) logger.log(Level.INFO, "[R] Got error code=" + internalResponse.getPayload().getErrorCode());
            ResponsePayload internalResponsePayload = internalResponse.getPayload();

            // prepare message that is sent back to the client
            internalResponsePayload.setCommand(requestPayload.getCommand());
            this.response.setPayload(internalResponsePayload);
            success = true;
        } catch (RequestTimeoutException e) {
            // if forward times out, determine new responsible node
            serverKeySpace.removeNode(responsibleNode);
            // String node = ServerApp.keySpace.getNode(this.requestPayload.getKey());

            StringBuilder sb = new StringBuilder();
            sb.append("[T] msgId=").append(toForward.getMessageIdAsString()).append(", node=").append(responsibleNode ).append(", size=")
                    .append(requestPayload.getValueLength()).append("\n: updating node list: ");

            String[] nodeStatus = serverKeySpace.getServerStatusList();

            for (String node : nodeStatus) {
                sb.append("\n").append(node);
            }
            logger.log(Level.INFO, sb.toString());
        } catch (IOException | PayloadCommandMissingException e) {
            e.printStackTrace();
        }
        return success;
    }


    /**
     * run method in thread, handles an incoming packet
     */
    @Override
    public void run() {
        RequestPayload requestPayload = this.request.getPayload();
        ResponsePayload responsePayload = this.response.getPayload();

        boolean success = false;
        try {
            request.parseData(requestPacket.getData());

            // send answer with the incoming messageId
            this.response.setMessageId(this.request.getMessageId());

            // save command to the responsePayload object
            responsePayload.setCommand(requestPayload.getCommand());
            COMMAND cmd = requestPayload.getCommand();
            switch (cmd) {
                case SHUTDOWN:
                    logger.log(Level.INFO, "[R] Shutdown. Good bye...");
                    System.exit(0);
                    break;
                case INVALID:
                case IS_ALIVE:
                case IS_ALIVE_OLD:
                case GET_PID:
                case DELETE_ALL:
                case PUT_REPLICA:
                case REMOVE_REPLICA:
                    localExecute();
                    break;
                default:
                    // execute locally if hashing returns local host or if local command
                    this.responsibleNode = serverKeySpace.getResponsibleNode(requestPayload.getKey());
                    String nodeAddress = InetAddress.getByName(this.responsibleNode).getHostAddress();
//                    if (this.externalListener || nodeAddress.equals(serverAddress.getHostAddress())) {
                    if (nodeAddress.equals(serverAddress.getHostAddress())) {
                        localExecute();
                    } else {
                        // forward for remote execution
                        int maxRetry = serverKeySpace.totalNodeCount();
                        int retryCount = 0;
                        while (!success && (retryCount < maxRetry)) {
                            success = tryForward(requestPayload);
                            retryCount++;
                        }
                    }
            }
        } catch (PayloadEmptyException e) {
            StringBuilder exception = new StringBuilder();
            exception.append(e.getClass().getName()).append(": payload empty");
            printDetailedErrorLog(exception);
        } catch (IOException e) {
            StringBuilder exception = new StringBuilder();
            exception.append(e.getClass().getName()).append(": key length does not match expectation");
            printDetailedErrorLog(exception);
        } catch (PayloadValueLengthException | PayloadNoKeyException | PayloadValueException e) {
            StringBuilder exception = new StringBuilder();
            exception.append(e.getClass().getName()).append(": incomplete request found");
            printDetailedErrorLog(exception);
            responsePayload.setErrorCode(REQUEST_INCOMPLETE);
        } catch (PayloadCommandInvalidException e) {
            StringBuilder exception = new StringBuilder();
            exception.append(e.getClass().getName()).append(": Invalid command found");
            printDetailedErrorLog(exception);
            responsePayload.setErrorCode(CMD_INVALID);
        } catch (PayloadValueLengthInvalidException e) {
            StringBuilder exception = new StringBuilder();
            exception.append(e.getClass().getName()).append(": Invalid getValue length found: ").append(requestPayload.getValueLength());
            printDetailedErrorLog(exception);
            responsePayload.setErrorCode(VAL_LEN_INVALID);
        } catch (PayloadValueLengthExceedsBuffer e) {
            StringBuilder exception = new StringBuilder();
            exception.append(e.getClass().getName()).append(": getValue length is longer than remaining buffer: ").append(requestPayload.getValueLength());
            printDetailedErrorLog(exception);
            responsePayload.setErrorCode(VAL_LEN_EXCEEDS_BUFFER);
        } catch (PayloadCommandMissingException e) {
            StringBuilder exception = new StringBuilder();
            exception.append(e.getClass().getName()).append(": Incomplete request: no command.");
            printDetailedErrorLog(exception);
            e.printStackTrace();
        } catch (PacketEmptyException | PacketIncompleteException e) {
            e.printStackTrace();
        } finally {
            // handle sending the response to the client
            if (this.response.getPayload().getErrorCode() != null) {
                try {
                    this.response.setDestinationAddress(this.requestPacket.getAddress());
                    this.response.setDestinationPort(this.requestPacket.getPort());
                    requestReplyProtocol.send(this.response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * build the payload for replication and appends to queue of replicationManager to be forwarded
     * @param payload
     */
    private void replicateOperation(RequestPayload payload) {
        // do not replicate if replication is disabled or it's neither PUT nor PUT_REPLICA
        // TODO: why replicate PUT_REPLICA?
        // TODO: is the if construct correct?
        if (!ServerApp.replicate) return;
        try {
            COMMAND cmd = payload.getCommand();
            if (!(cmd.equals(COMMAND.PUT) || cmd.equals(COMMAND.PUT_REPLICA))) return;
        } catch (PayloadCommandMissingException e) {
            e.printStackTrace();
        }

        try {
            Request replicationRequest = new Request(logger);
            StringBuilder sb = new StringBuilder();

            sb.append("Prepare replication:");

            RequestPayload replicationRequestPayload = new RequestPayload(logger);

            switch (payload.getCommand()) {
                case PUT:
                    replicationRequestPayload.setCommand(COMMAND.PUT_REPLICA);
                    replicationRequestPayload.setKey(payload.getKey());
                    replicationRequestPayload.setValue(payload.getValue());

                    sb.append(" key=").append(replicationRequestPayload.getKeyAsString());
                    break;
                case REMOVE:
                    replicationRequestPayload.setCommand(COMMAND.REMOVE_REPLICA);
                    replicationRequestPayload.setKey(payload.getKey());

                    sb.append(", key=").append(replicationRequestPayload.getKeyAsString());
                    break;
            }

            // append to replication queue
            replicationRequest.setPayload(replicationRequestPayload);
            replicationManager.addReplicationRequest(replicationRequest);

            logger.log(Level.INFO, sb.toString());

        } catch (PayloadCommandMissingException | KeyLenMismatchException | PayloadValueTooLongException e) {
            e.printStackTrace();
        }
    }

    /**
     * Look up the cache for an object with the same messageId
     * @throws PacketRetryException
     */
    private void tryGetResponsePayloadFromCache() throws PacketRetryException {
        try {
            String messageId = request.getMessageIdAsString();
            CachedObject found = cache.get(messageId);
            if (found.isProcessed()) {
                logger.log(Level.INFO, "[C] msgId=" + messageId);
                this.response.setPayload(found.getResponsePayload());
                throw new PacketRetryException();
            }
        } catch (NullPointerException e) {
            // no duplicate
        }
    }

    /**
     * print detailed error messages if required
     * @param sb
     */
    private void printDetailedErrorLog(StringBuilder sb) {
        try {
            RequestPayload payload = this.request.getPayload();
            sb.append("\tIncoming port=").append(serverPort).append("\n");
            sb.append("\tmsg_id=").append(this.request.getMessageIdAsString()).append("\n");
            sb.append("\tcommand=").append(payload.getCommand().toString()).append("\n");
            sb.append("\tkey=").append(payload.getKeyAsString()).append("\n");
            sb.append("\tpayload=").append(payload.toString());
        } catch (PayloadCommandMissingException err) {
            sb.append("error getting command. Details might not be complete.");
        } finally {
            logger.log(Level.INFO, sb.toString());
        }
    }
}
