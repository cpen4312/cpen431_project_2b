package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.Constants;
import com.s73504152.CPEN431.A5.Interfaces.Message;

import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Simon on 02-03-2016.
 */
public class RequestReplyProtocol {
    private final Logger logger;
    private InetAddress serverAddress;

    public RequestReplyProtocol(Logger logger) {
        this.logger = logger;
        try {
            this.serverAddress = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public Response sendAndReceive(Message message, int timeout, boolean retry) throws IOException, RequestTimeoutException {
        DatagramSocket socket = new DatagramSocket();
        // TODO: in local testing, the source address will be localhost. Change this?
        DatagramPacket packet;
        byte[] responseBytes = new byte[Constants.PACKET_MAX_LEN];

        int tries = 0;
        int maxTries = 1;
        if (retry) maxTries = Constants.SERVER_INTERNAL_MAX_SEND_RETRY;

        while (tries < maxTries) {
            try {
                StringBuilder sb = new StringBuilder();
                if (ServerApp.debug) {
                    sb.append("[S] ");
                    sb.append("msgId=").append(message.getMessageIdAsString());
                }

                // send request to server
                message.setSourcePort(socket.getLocalPort());
                message.setSourceAddress(serverAddress);
                byte[] messageBytes = message.getPacketBytes();
                packet = new DatagramPacket(messageBytes, messageBytes.length, message.getDestinationAddress(), message.getDestinationPort());
                socket.send(packet);

                // receive response
                packet = new DatagramPacket(responseBytes, responseBytes.length);
                socket.setSoTimeout(timeout);
                socket.receive(packet);

                Response response = new Response(logger);
                response.parseData(packet.getData());

                // collect ID and compare it to request
                String requestId = message.getMessageIdAsString();
                String responseId = response.getMessageIdAsString();
                if (!requestId.equals(responseId)) {
                    logger.log(Level.INFO, "ERR: Request ID / receive ID mismatch: " + requestId + " != " + responseId);
                    continue;
                }

                if (ServerApp.debug)
                    // logger.log(Level.INFO, "Status: Success after " + (tries + 1) + " tries");

                socket.close();
                return response;
            } catch (SocketTimeoutException e) {
                tries++;
                timeout *= 2;
            } catch (PayloadIncompleteException | PayloadValueLengthInvalidException | PayloadNoKeyException | PayloadCommandInvalidException | PacketEmptyException | PacketIncompleteException | PayloadValueLengthExceedsBuffer | PayloadValueLengthException | PayloadNoCommandException | PayloadCommandMissingException | PayloadValueException | PayloadEmptyException e) {
                e.printStackTrace();
            }
        }
        socket.close();
        throw new RequestTimeoutException();
    }

    public void send(Message message) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (DatagramSocket socket = new DatagramSocket()) {
            ResponsePayload payload = ((Response) message).getPayload();

            if (ServerApp.debug) {
                sb.append("[S] ");
                sb.append("msgId=").append(message.getMessageIdAsString());
                sb.append(", cmd=").append(payload.getCommand());
                sb.append(", err=").append(payload.getErrorCode());
            }

            // send request to server
            message.setSourcePort(socket.getLocalPort());
            message.setSourceAddress(serverAddress);
            byte[] messageBytes = message.getPacketBytes();
            DatagramPacket packet = new DatagramPacket(messageBytes, messageBytes.length, message.getDestinationAddress(), message.getDestinationPort());
            socket.send(packet);
        } catch (PayloadIncompleteException | PayloadValueLengthException | PayloadNoCommandException | PayloadCommandMissingException | PayloadValueException | PayloadEmptyException e1) {
            e1.printStackTrace();
        } finally {
            if (ServerApp.debug) logger.log(Level.INFO, sb.toString());
        }
    }
}

