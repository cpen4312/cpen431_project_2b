package com.s73504152.CPEN431.A5;

import com.google.code.gossip.*;
import com.google.code.gossip.event.GossipListener;
import com.google.code.gossip.event.GossipState;
import com.s73504152.CPEN431.A5.Helper.Constants;
import com.s73504152.CPEN431.A5.Helper.SingleLineFormatter;
import org.apache.log4j.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by chbndrhnns on 29.03.2016.
 */
public class GossipManager implements Runnable {
    private final Logger logger = Logger.getLogger(GossipManager.class.getName());
    private ServerApp serverApp;
    private List<GossipMember> gossipMembers = new ArrayList<>();
    private GossipService gossipService;
    private GossipSettings settings;
    private int gossipPort;

    public GossipManager(ServerApp serverApp) {
        setupLogger(serverApp.address);
        // log4j init stuff
        org.apache.log4j.Logger root = org.apache.log4j.Logger.getRootLogger();
        root.setLevel(org.apache.log4j.Level.OFF);
        if (!root.getAllAppenders().hasMoreElements()) {
            String host = null;
            try {
                if (ServerApp.localTest) {
                    host = InetAddress.getLocalHost().getHostAddress();
                }
                else {
                    host = InetAddress.getLocalHost().getHostName();
            }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            // root.addAppender(new ConsoleAppender(new PatternLayout("[" + host.getHostName() + "] - %r [%t] %p %c %x - %m%n")));
            root.addAppender(new ConsoleAppender(new PatternLayout("[" + serverApp.address.getHostAddress() + "] %d{HH:mm:ss.SSS} - [%t] - %m%n")));
        }

        this.serverApp = serverApp;
        this.gossipPort = serverApp.externalServerPort + Constants.GOSSIP_PORT_OFFSET;
        settings = new GossipSettings();
        settings.setGossipTimeout(Constants.GOSSIP_INTERVAL);
        settings.setCleanupInterval(Constants.GOSSIP_CLEANUP_INTERVAL);
    }


    public void buildStartupList() {
        String[] nodes = serverApp.keySpace.getAllNodes();

        if (nodes != null) {
            for (String node : nodes) {
                RemoteGossipMember m = new RemoteGossipMember(node, serverApp.keySpace.getPort(node) + Constants.GOSSIP_PORT_OFFSET, node);
                gossipMembers.add(m);
            }
        }
    }


    @Override
    public void run() {
        try {
            String hostAddress = serverApp.address.getHostAddress();
            this.gossipService = new GossipService(hostAddress, gossipPort, hostAddress, gossipMembers, settings, new GossipListener() {
                @Override
                public void gossipEvent(GossipMember member, GossipState state) {
                    logger.log(Level.INFO, "Listener on: " + hostAddress + ":" +
                            gossipPort +  " - " + member +" "+ state);
                }
            });
            StringBuilder sb = new StringBuilder();
            sb.append("Gossip protocol started on ").append(hostAddress).append(":").append(gossipPort);
            logger.log(Level.INFO, sb.toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        gossipService.start();
    }

    /*
    setup custom Formatter for logging
    */
    public void setupLogger(InetAddress address)  {
        logger.setUseParentHandlers(false);
        if (logger.getHandlers().length == 0) {
            try {
                Handler cons = new ConsoleHandler();
                cons.setLevel(Constants.LOGGER_LEVEL);
                cons.setFormatter(new SingleLineFormatter(address));
                logger.addHandler(cons);

                Handler file = new FileHandler("logs/gossip.%u.%g.log", Constants.LOGGER_MAX_SIZE_PER_LOGFILE, Constants.LOGGER_MAX_NR_OF_LOGFILES, true);
                file.setLevel(Constants.LOGGER_LEVEL);
                file.setFormatter(new SingleLineFormatter(address));
                logger.addHandler(file);
            }
            catch (IOException e) {
                System.out.println("ERR: Could not initialize log file.");
            }
        }
    }
}
