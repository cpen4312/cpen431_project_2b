package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.OutOfHeapSpaceException;
import com.s73504152.CPEN431.A5.Helper.Constants;
import com.s73504152.CPEN431.A5.Helper.MemoryChecker;
import com.s73504152.CPEN431.A5.Helper.PacketReceiver;
import com.s73504152.CPEN431.A5.Helper.SingleLineFormatter;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.*;
import java.util.HashMap;
import java.util.logging.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by chbndrhnns on 14.01.2016.
 * based upon samples from "Learning Network Programming with Java",
 * https://www.packtpub.com/application-development/learning-network-programming-java
 */
public class ServerApp {
    public static boolean debug = false;
    public static boolean localTest = false;
    public static boolean master = false;
    public static boolean replicate = true;

    public static final Logger logger = Logger.getLogger(ServerApp.class.getName());
    private GossipManager gossipManager;
    private DatagramSocket socket = null;
    public Storage dataStorage;
    public Cache cache;
    public MemoryChecker mem;
    public InetAddress address;
    public int externalServerPort;
    public int internalServerPort;

    public PacketReceiver externalPacketReceiver;
    public PacketReceiver internalPacketReceiver;
    public ConsistentHashing keySpace;
    public ReplicationManager replicationManager;
    public HashMap<String, Integer> masterList;

    /*
    constructor
     */
    public ServerApp(InetAddress address, int externalServerPort, boolean debug, boolean localTest, boolean master, boolean replicate) {
        ServerApp.debug = debug;
        ServerApp.replicate = replicate;
        ServerApp.localTest = localTest;
        ServerApp.master = master;
        this.address = address;
        this.externalServerPort = externalServerPort;
        this.internalServerPort = externalServerPort + Constants.SERVER_INTERNAL_PORT_OFFSET;

        setupLogger(this.address);
        logger.log(Level.INFO, "ServerApp starting...");

        StringBuilder sb = new StringBuilder();
        sb.append("Options: ");
        sb.append("debug=").append(debug);
        sb.append(", replicate=").append(replicate);
        sb.append(", localTest=").append(localTest);
        sb.append(", master=").append(master);
        logger.log(Level.INFO, sb.toString());

        this.dataStorage = new Storage(logger);
        this.keySpace = new ConsistentHashing(this, logger);
        this.gossipManager = new GossipManager(this);
        this.masterList = new HashMap<>();

        try {
            if (ServerApp.localTest) {
                keySpace.initNodes(Constants.SERVER_NODES_TEST);
            } else {
                keySpace.initNodes(Constants.SERVER_NODES_STAGING);
            }
        } catch (IOException e) {
            logger.log(Level.INFO, "nodes file not found.");
            System.exit(0);
        }

        this.cache = new Cache(Constants.CACHE_TIME_TO_LIVE, Constants.CACHE_CLEANUP_CYCLE, logger);
        this.mem = new MemoryChecker(dataStorage, logger, Constants.SERVER_MEM_CLEANUP_CYLCE);

        try {
            mem.checkServerResources();
        } catch (OutOfHeapSpaceException e) {
            e.printStackTrace();
        }
    }

    public void runServerApp() {
        try {
            ReplicationManager replicationManager = new ReplicationManager(logger, this);
            (new Thread(replicationManager)).start();

            this.externalPacketReceiver = new PacketReceiver(this, address, this.externalServerPort, true, this.dataStorage, this.cache, this.keySpace, replicationManager, logger);
            this.internalPacketReceiver = new PacketReceiver(this, address, this.internalServerPort, false, this.dataStorage, this.cache, this.keySpace, replicationManager, logger);
            (new Thread(this.externalPacketReceiver)).start();
            (new Thread(this.internalPacketReceiver)).start();

            // start gossiping thread
            gossipManager.buildStartupList();
            // gossipManager.run();

            //NodeDecider run
            if(master){
                //run through NodeDecider
            }else{
                //run normally, intervals of pinging
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    setup custom Formatter for logging
    */
    public static void setupLogger (InetAddress address){
            logger.setUseParentHandlers(false);
            if (logger.getHandlers().length == 0) {
                try {
                    Handler cons = new ConsoleHandler();
                    cons.setLevel(Constants.LOGGER_LEVEL);
                    cons.setFormatter(new SingleLineFormatter(address));
                    logger.addHandler(cons);

                    Handler file = new FileHandler("logs/myServer.%u.%g.log", Constants.LOGGER_MAX_SIZE_PER_LOGFILE, Constants.LOGGER_MAX_NR_OF_LOGFILES, true);
                    file.setLevel(Constants.LOGGER_LEVEL);
                    file.setFormatter(new SingleLineFormatter(address));
                    logger.addHandler(file);
                } catch (IOException e) {
                    System.out.println("ERR: Could not initialize log file.");
                }
            }
        }


    public static int getPID() {
        RuntimeMXBean rtb = ManagementFactory.getRuntimeMXBean();
        String processName = rtb.getName();
        Integer result = null;

            /* tested on: */
            /* - windows xp sp 2, java 1.5.0_13 */
            /* - mac os x 10.4.10, java 1.5.0 */
            /* - debian linux, java 1.5.0_13 */
            /* all return pid@host, e.g 2204@antonius */

        Pattern pattern = Pattern.compile("^([0-9]+)@.+$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(processName);
        if (matcher.matches()) {
            result = new Integer(Integer.parseInt(matcher.group(1)));
        }
        return result;
    }
}
