package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.Constants;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Simon on 30-03-2016.
 */
public class ReplicationManager implements Runnable {
    private ServerApp serverApp;
    private Logger logger;
    private final ConsistentHashing serverKeySpace;
    ConcurrentLinkedQueue<Request> requestQueue;

    public ReplicationManager(Logger logger, ServerApp serverApp) {
        this.logger = logger;
        this.serverKeySpace = serverApp.keySpace;
        this.serverApp = serverApp;

        requestQueue = new ConcurrentLinkedQueue<>();

    }

    @Override
    public synchronized void run() {
        boolean running = true;
        while (running) {
            if (!requestQueue.isEmpty()) {
                Request req = requestQueue.poll();
                forwardRequest(req);
            }
            try {
                this.wait();
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }
        }
    }

    public synchronized void addReplicationRequest(Request req) {
        requestQueue.add(req);
        this.notify();
    }

    private void forwardRequest(Request request) {
        RequestPayload payload = request.getPayload();
        StringBuilder sb = new StringBuilder();
        LinkedList<String> successors = serverKeySpace.getSuccessors(payload.getKey(), false, false);

        if (ServerApp.debug) {
            if (ServerApp.localTest) sb.append("[").append(serverApp.address.getHostAddress()).append("] - ");
            sb.append("[+] key=").append(payload.getKeyAsString());
            sb.append(", destination_nodes=");
        }

        RequestReplyProtocol rrp = new RequestReplyProtocol(logger);

        for (String successor : successors) {

            if (ServerApp.debug) { sb.append(successor).append(": "); }
            try {
                request.setDestinationAddress(InetAddress.getByName(successor));
                request.setDestinationPort(serverKeySpace.getPort(successor)  + Constants.SERVER_INTERNAL_PORT_OFFSET);
                // called with messageId = null because new one is to be generated
                Response response = rrp.sendAndReceive(request, Constants.SERVER_RECEIVE_TIMEOUT, true);

                // TODO: determine what to do with response. responsible node should take action on the feedback
                /*
                ResponsePayload localResponsePayload = parseResponsePacket(localResponse);
                localResponsePayload.setCommand(this.requestPayload.getCommand());
                this.sendResponse(localResponsePayload.getPayloadBytes());*/

                if (ServerApp.debug) { sb.append("OK, "); }

            } catch (RequestTimeoutException e) {
                if (ServerApp.debug) { sb.append("Timeout"); }
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (ServerApp.debug) { logger.log(Level.INFO, sb.toString()); }
    }

    /**
     * This method is called when a node dies, and initiates a heal of the three sets of lost keys
     * depending on the current node. It is performed for all virtual nodes of the dead node
     * @param deadNode the node that just died
     */
    //TODO: change type?
    public void deadNode(String deadNode){
        String me = serverApp.address.getHostAddress();

        Set<ByteBuffer> keySet = serverApp.dataStorage.getKeySet(); //TODO: use constant space instead, iterating through
        // for all virtual nodes
        for (int i = 1; i < Constants.NUMBER_OF_VIRTUAL_NODES + 1; i++) {
            String virtualNode = (deadNode + "#" + i);

            // both lists contains virtual node numbers
            LinkedList<String> successorsOfDeadNode = serverKeySpace.getSuccessors(virtualNode.getBytes(), true, true);
            LinkedList<String> predecessorsOfDeadNode = serverKeySpace.getPredecessors(virtualNode.getBytes(), true, true);

            //TODO: change it, such that the same virtual node in a row is only worked once
            // if immediate successor, replicate originals of dead node
            if (me.equals(successorsOfDeadNode.get(0).split("#")[0])){
                heal(deadNode, successorsOfDeadNode.get(0), virtualNode, keySet);
            }
            // if immediate predecessor or second predecessor, replicate own originals
            if (me.equals(predecessorsOfDeadNode.get(0).split("#")[0]) ) {
                heal(deadNode, predecessorsOfDeadNode.get(0), predecessorsOfDeadNode.get(0), keySet);
            }
            if (me.equals(predecessorsOfDeadNode.get(1).split("#")[0])) {
                heal(deadNode, predecessorsOfDeadNode.get(1), predecessorsOfDeadNode.get(1), keySet);
            }
        }
    }

    /**
     * Heals either originals or replicas (depends on toNode). Walks through own KVStore to determine which keys has been lost
     * and forwards to nodes that now serves as new replicas
     * @param deadNode for debug
     * @param me in the form hostname#VirtualNumber
     * @param toNode the node to heal until. If dead node is specified, originals are heals, if me then replicas are healed
     * @param keySet
     */
    private void heal(String deadNode, String me, String toNode, Set<ByteBuffer> keySet) {
        StringBuilder sb = new StringBuilder();
        sb.append("[" + me + "] heal: initializing heal, deadNode: " + deadNode);
        logger.log(Level.INFO, sb.toString());

        // get immediate pred. along with virtual node number
        String immediatePredecessorOfMe = serverKeySpace.getPredecessors(me.getBytes(), false, true).get(0);

        // get second succ. without virtual node number
        String secondSuccessorOfMe = serverKeySpace.getSuccessors(me.getBytes(), false, false).get(1);

        Integer fromKey = serverKeySpace.hashFunction(immediatePredecessorOfMe.getBytes());
        Integer toKey = serverKeySpace.hashFunction(toNode.getBytes());

        LinkedList<ByteBuffer> keysToForward = new LinkedList<>();

        for (ByteBuffer key : keySet) {
            Integer hashOfKey = serverKeySpace.hashFunction(key.array());
            // normal case
            if (fromKey <= hashOfKey && hashOfKey <= toKey) {
                keysToForward.add(key);
            }
            // keySpace overlap case
            if (fromKey > toKey && (toKey <= hashOfKey || hashOfKey <= toKey)) {
                keysToForward.add(key);
            }
        }

        if (keysToForward.size() > 0) {
            logger.log(Level.INFO, "[" + me + "] heal: forwarding " + keysToForward.size() + " keys to " + secondSuccessorOfMe);
            forwardRequestUponHeal(secondSuccessorOfMe, keysToForward);
        } else {
            logger.log(Level.INFO, "[" + me + "] heal: no keys to forward");
        }
    }

    private void forwardRequestUponHeal(String targetNode, LinkedList<ByteBuffer> keysToForward) {
        Healer healer = new Healer(serverApp, logger, targetNode, keysToForward);
        (new Thread(healer)).start();
    }


}
