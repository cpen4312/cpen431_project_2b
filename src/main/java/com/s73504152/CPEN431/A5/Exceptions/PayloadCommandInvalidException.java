package com.s73504152.CPEN431.A5.Exceptions;

public class PayloadCommandInvalidException extends Exception {
    public PayloadCommandInvalidException() {}
    public PayloadCommandInvalidException(String message) { super(message); }
}
