package com.s73504152.CPEN431.A5.Exceptions;

public class RequestTimeoutException extends Exception {
    public RequestTimeoutException() {}
    public RequestTimeoutException(String m) { super(m); }
}
