package com.s73504152.CPEN431.A5.Exceptions;

/**
 * Created by Johannes on 10.03.2016.
 */
public class PacketBuildException extends Throwable {
    public PacketBuildException() {}
    public PacketBuildException(String m) { super(m); }

}
