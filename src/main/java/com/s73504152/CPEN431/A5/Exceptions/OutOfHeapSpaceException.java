package com.s73504152.CPEN431.A5.Exceptions;

/**
 * Created by Johannes on 20.01.2016.
 */
public class OutOfHeapSpaceException extends Exception {
    public OutOfHeapSpaceException() {}
    public OutOfHeapSpaceException(String m) { super(m); }
}
