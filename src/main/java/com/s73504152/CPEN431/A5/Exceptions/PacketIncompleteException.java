package com.s73504152.CPEN431.A5.Exceptions;

/**
 * Created by Johannes on 12.04.2016.
 */
public class PacketIncompleteException extends Exception {
    public PacketIncompleteException() {}
    public PacketIncompleteException(String m) { super(m); }
}
