package com.s73504152.CPEN431.A5.Exceptions;

/**
 * Created by Johannes on 14.01.2016.
 */

public class KeyLenMismatchException extends Exception
{
    //Parameterless Constructor
    public KeyLenMismatchException() {}

    //Constructor that accepts a message
    public KeyLenMismatchException(String message)
    {
        super(message);
    }
}
