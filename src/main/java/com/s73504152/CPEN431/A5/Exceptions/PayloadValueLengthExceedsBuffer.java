package com.s73504152.CPEN431.A5.Exceptions;

/**
 * Created by Johannes on 26.01.2016.
 */
public class PayloadValueLengthExceedsBuffer extends Exception {
    public PayloadValueLengthExceedsBuffer() {}
    public PayloadValueLengthExceedsBuffer(String m) { super(m); }
}
