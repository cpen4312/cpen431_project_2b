package com.s73504152.CPEN431.A5.Exceptions;

/**
 * Created by Simon on 31-03-2016.
 */
public class ReplicationRequestBuildException extends Throwable {
    public ReplicationRequestBuildException() {}
    public ReplicationRequestBuildException(String m) { super(m); }
}
