package com.s73504152.CPEN431.A5.Exceptions;

/**
 * Created by Johannes on 26.01.2016.
 */
public class PayloadIncompleteException extends Exception {
    public PayloadIncompleteException() {}
    public PayloadIncompleteException(String m) { super(m); }
}
