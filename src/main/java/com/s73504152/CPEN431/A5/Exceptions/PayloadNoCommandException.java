package com.s73504152.CPEN431.A5.Exceptions;

public class PayloadNoCommandException extends Exception {
    public PayloadNoCommandException() {}
    public PayloadNoCommandException(String message) { super(message); }
}
