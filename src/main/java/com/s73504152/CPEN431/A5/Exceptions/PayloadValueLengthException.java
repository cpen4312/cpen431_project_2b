package com.s73504152.CPEN431.A5.Exceptions;

public class PayloadValueLengthException extends Exception {
    public PayloadValueLengthException() {}
    public PayloadValueLengthException(String message) { super(message); }
}
