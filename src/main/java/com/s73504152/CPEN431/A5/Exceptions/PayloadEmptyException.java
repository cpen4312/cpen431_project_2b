package com.s73504152.CPEN431.A5.Exceptions;

public class PayloadEmptyException extends Exception
{
    //Parameterless Constructor

    public PayloadEmptyException()
    {

    }
    //Constructor that accepts a message
    public PayloadEmptyException(String message)
    {
        super(message);
    }
}
