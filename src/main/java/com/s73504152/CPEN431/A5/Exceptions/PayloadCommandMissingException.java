package com.s73504152.CPEN431.A5.Exceptions;

public class PayloadCommandMissingException extends Exception {
    public PayloadCommandMissingException() {}
    public PayloadCommandMissingException(String message) { super(message); }
}
