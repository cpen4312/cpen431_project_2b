package com.s73504152.CPEN431.A5.Exceptions;

public class PayloadValueException extends Exception {
    public PayloadValueException() {}
    public PayloadValueException(String message) { super(message); }
}
