package com.s73504152.CPEN431.A5.Exceptions;

public class StorageEmptyException extends Exception
{
    public StorageEmptyException() {}
    public StorageEmptyException(String m) { super(m); }
}

