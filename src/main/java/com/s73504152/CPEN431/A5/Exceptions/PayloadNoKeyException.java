package com.s73504152.CPEN431.A5.Exceptions;

public class PayloadNoKeyException extends Exception {
    public PayloadNoKeyException() {}
    public PayloadNoKeyException(String message) { super(message); }
}
