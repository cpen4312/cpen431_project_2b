package com.s73504152.CPEN431.A5.Exceptions;

public class PayloadValueTooLongException extends Exception {
    public PayloadValueTooLongException() {}
    public PayloadValueTooLongException(String message) { super(message); }
}
