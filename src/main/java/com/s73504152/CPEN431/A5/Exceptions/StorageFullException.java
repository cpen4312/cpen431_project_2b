package com.s73504152.CPEN431.A5.Exceptions;

/**
 * Created by Johannes on 20.01.2016.
 */
public class StorageFullException extends Exception{
    public StorageFullException() {}
    public StorageFullException(String m) { super(m); }
}
