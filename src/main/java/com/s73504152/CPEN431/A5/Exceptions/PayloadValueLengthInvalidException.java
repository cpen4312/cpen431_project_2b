package com.s73504152.CPEN431.A5.Exceptions;

public class PayloadValueLengthInvalidException extends Exception {
    public PayloadValueLengthInvalidException() {}
    public PayloadValueLengthInvalidException(String m) { super(m); }
}
