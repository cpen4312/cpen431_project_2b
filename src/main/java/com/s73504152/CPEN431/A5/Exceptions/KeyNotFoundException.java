package com.s73504152.CPEN431.A5.Exceptions;

public class KeyNotFoundException extends Exception
{
    //Parameterless Constructor
    public KeyNotFoundException() {}

    //Constructor that accepts a message
    public KeyNotFoundException(String message)
    {
        super(message);
    }
}
