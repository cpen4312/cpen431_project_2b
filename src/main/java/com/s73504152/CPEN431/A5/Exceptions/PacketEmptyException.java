package com.s73504152.CPEN431.A5.Exceptions;

/**
 * Created by Johannes on 19.01.2016.
 */
public class PacketEmptyException extends Exception {
    public PacketEmptyException() {}
    public PacketEmptyException(String m) { super(m); }
}
