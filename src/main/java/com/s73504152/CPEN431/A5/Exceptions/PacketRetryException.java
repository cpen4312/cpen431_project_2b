package com.s73504152.CPEN431.A5.Exceptions;

/**
 * Created by Johannes on 26.01.2016.
 */
public class PacketRetryException extends Exception {
    public PacketRetryException() {}
    public PacketRetryException(String m) { super(m); }
}
