package com.s73504152.CPEN431.A5.Exceptions;

/**
 * Created by Johannes on 04.03.2016.
 */
public class RequestIncompleteException extends Exception {
    public RequestIncompleteException() {}
    public RequestIncompleteException(String m) { super(m); }
}
