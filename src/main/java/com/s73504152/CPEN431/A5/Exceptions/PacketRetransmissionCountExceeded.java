package com.s73504152.CPEN431.A5.Exceptions;

/**
 * Created by Johannes on 29.01.2016.
 */
public class PacketRetransmissionCountExceeded extends Exception {
    public PacketRetransmissionCountExceeded() { }
    public PacketRetransmissionCountExceeded(String m) { super(m); }
}
