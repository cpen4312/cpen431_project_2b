package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Helper.Constants;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by chbndrhnns on 27.01.2016.
 */
public class ServerAppRunner {

    public static void main(String[] args) {
        int i = 0;

        boolean debug = false;
        boolean localTest = false;
        boolean master = false;
        boolean replicate = true;

        int port = Constants.SERVER_PUBLIC_PORT;
        String host = null;
        try {
            while (i < args.length) {
                // set port not needed at the moment
                if (args[i].equals("-p")) {
                    port = Short.parseShort(args[i + 1]);
                    i += 2;
                    continue;
                }
                if (args[i].equals("-h")) {
                    host = (args[i + 1]);
                    i += 2;
                    continue;
                }
                if (args[i].equals("-norep")) {
                    replicate = false;
                    i++;
                    continue;
                }
                if (args[i].equals("-debug")) {
                    debug = true;
                    i++;
                    continue;
                }
                if (args[i].equals("-master")){
                    master = true;
                    i++;
                    continue;
                }
                if (args[i].equals("-localtest")) {
                    localTest = true;
                    i++;
                    continue;
                }
                if (args.length > i) {
                    throw new Exception("Wrong set of arguments");
                }
            }

            BufferedReader in;
            try {
                if (localTest) {
                    in = new BufferedReader(new FileReader(Constants.SERVER_NODES_TEST));
                    String line;
                    int localPort;
                    while ((line = in.readLine()) != null) {

                        if (line.length() != 0) {
                            host = line.split(":")[0];
                            InetAddress serverAddress = InetAddress.getByName(host);
                            localPort = Integer.parseInt(line.split(":")[1]);
                            new ServerApp(serverAddress, localPort, debug, localTest, master, replicate).runServerApp();
                        }
                    }
                }
                else {
                    // find local hostname
                    if (host == null) {
                        host = InetAddress.getLocalHost().getHostAddress();
                    }
                    InetAddress hostName = InetAddress.getByName(host);
                    new ServerApp(hostName, port, debug, localTest, master, replicate).runServerApp();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(
                    e +
                            "\n" +
                            " Usage: java <package>/ServerApp <options> \n" +
                            "   Options: \n" +
                            "     -h  host       set the hostname that the server should run on" +
                            "     -p port        set the external port, if not set default is chosen\n" +
                            "     -norep         disable replication\n" +
                            "     -debug         start server in debug mode\n" +
                            "     -localtest     start server in local test mode (parses nodes_local.txt"


            );
            System.exit(1);
        }

    }


}

