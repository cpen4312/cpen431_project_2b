package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.*;

import static com.s73504152.CPEN431.A5.Helper.ERROR.OK;

/**
 * Created by chbndrhnns on 27.01.2016.
 */
/**
 * Class that keeps the payload of a response
 */
public class ResponsePayload {
    private final Logger logger;

    private ERROR errorCode;
    private COMMAND command;
    private short valueLength = -1;
    private byte[] value;

    public void setErrorCode(ERROR code) {
        this.errorCode = code;
    }

    public ERROR getErrorCode() {
        return errorCode;
    }

    public void setCommand(COMMAND command) {
        this.command = command;
    }

    public COMMAND getCommand() {
        return command;
    }


    public byte[] getValue() {
        return value;
    }

    public void setValue(byte[] value) {
        this.value = value;
        this.valueLength = (short) value.length;
    }

    public String getValueAsString() {
        if (this.value == null) {
            return "";
        }
        return javax.xml.bind.DatatypeConverter.printHexBinary(this.value);
    }

    /*
    constructor
     */
    public ResponsePayload(Logger logger) {
        this.logger = logger;
    }

    /*
    creates a response payload
     */
    public byte[] getPayloadBytes() throws IOException, PayloadEmptyException, PayloadIncompleteException {
        if (errorCode == null)
            throw new PayloadIncompleteException("Error code not set");
        if (command == null)
            throw new PayloadIncompleteException("Command not set. necessary to determine which action to take.");

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] payload;
        try {
            outputStream.write(ByteArrayHelper.intToByteArray(errorCode.getId()));
            byte[] len;

            if (errorCode.equals(OK)) {
                switch (command) {
                    case PUT:
                        break;
                    case GET:
                        if (value == null) {
                            throw new PayloadValueException("value not set");
                        }
                        len = ByteArrayHelper.shortToByteArray(valueLength);
                        outputStream.write(len);
                        outputStream.write(value);
                        break;
                    case REMOVE:
                        break;
                    case SHUTDOWN:
                        break;
                    case DELETE_ALL:
                        break;
                    case IS_ALIVE:
                        break;
                    case GET_PID:
                        len = ByteArrayHelper.intToByteArray(valueLength);
                        outputStream.write(len);
                        outputStream.write(value);
                        break;
                }
            }
        } catch (PayloadValueException | OutOfMemoryError | NullPointerException e) {
            e.printStackTrace();
        } finally {
            payload = outputStream.toByteArray();
            //noinspection ThrowFromFinallyBlock
            outputStream.close();
        }

        if (payload == null) {
            throw new PayloadEmptyException();
        }
        return payload;
    }

    /*
parses a given RequestPayload (mostly for test cases)
 */
    public void parseByteArray(byte[] payload, byte[] messageId) throws PayloadValueLengthExceedsBuffer, PayloadEmptyException, PayloadIncompleteException {
        if (payload == null) throw new PayloadEmptyException();
        if (payload.length == 0) throw new PayloadEmptyException();

        byte[] errByte = new byte[Constants.CMD_LEN];
        byte[] valLen = new byte[Constants.VAL_LEN_LEN];
        // byte[] key;

        int offset = 0;

        // get err code
        System.arraycopy(payload, 0, errByte, 0, Constants.CMD_LEN);
        this.errorCode = ERROR.valueOf(errByte[0]);
        if (errorCode == null) {
            // throw new PayloadCommandInvalidException();
            if (ServerApp.debug) logger.log(Level.INFO, "no error code found.");
        }

        offset += Constants.CMD_LEN;

        StringBuilder sb = new StringBuilder();
        if (ServerApp.debug) {
            sb.append("[R] msgId=");
            if (messageId != null) sb.append(ByteArrayHelper.byteArrayToString(messageId));
            sb.append(", err=");
            if (null != errorCode) sb.append(errorCode.toString());
        }

        switch (errorCode) {
            case OK:
                // only try to read if bytes are left
                if (payload.length - offset > 0) {
                    try {
                        // get getValue length field and check it
                        System.arraycopy(payload, offset, valLen, 0, Constants.VAL_LEN_LEN);
                        this.valueLength = (short) ByteOrder.leb2int(valLen, 0, 2);
                        offset += Constants.VAL_LEN_LEN;

                        if (this.valueLength > (payload.length - offset)) throw new PayloadValueLengthExceedsBuffer();

                        // get getValue
                        this.value = new byte[this.valueLength];
                        System.arraycopy(payload, offset, this.value, 0, this.valueLength);

                        // log
                        if (ServerApp.debug) {
                            sb.append(", len=");
                            sb.append(this.valueLength);
                            sb.append(", val=");
                            sb.append(ByteArrayHelper.byteArrayToString(this.value).substring(0, Math.min(this.valueLength, Constants.LOGGER_PRINT_MAX_VAL_LEN)));
                            sb.append("[...]");
                        }
                    } catch (NegativeArraySizeException | NullPointerException e) {
                        logger.log(Level.INFO, "ERR: Something went wrong. Input data: '" +
                                ByteArrayHelper.byteArrayToString(payload).substring(0,
                                        Math.min(payload.length, Constants.LOGGER_PRINT_MAX_PAYLOAD_LEN)) + "'");
                        throw new PayloadIncompleteException("Payload probably incomplete.");
                    }
                }
                break;
        }
        if (ServerApp.debug) {
            logger.log(Level.INFO, sb.toString());
        }
    }


}
