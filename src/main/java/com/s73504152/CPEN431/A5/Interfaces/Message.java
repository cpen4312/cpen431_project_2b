package com.s73504152.CPEN431.A5.Interfaces;

import com.s73504152.CPEN431.A5.Exceptions.*;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by chbndrhnns on 06.04.2016.
 */
public interface Message {
    void setDestinationAddress(InetAddress destinationAddress);
    InetAddress getDestinationAddress();

    void setDestinationPort(int destinationPort);
    int getDestinationPort();

    void setSourceAddress(InetAddress sourceAddress);
    InetAddress getSourceAddress();

    void setSourcePort(int sourcePort);
    int getSourcePort();

    byte[] getMessageId();
    String getMessageIdAsString();

    void setPayload(Object payload);
    Object getPayload();

    byte[] getPacketBytes() throws IOException, PayloadIncompleteException, PayloadEmptyException, PayloadValueException, PayloadCommandMissingException, PayloadNoCommandException, PayloadValueLengthException;

    void parseData(byte[] raw) throws IOException, PayloadCommandInvalidException, PayloadValueLengthInvalidException, PayloadValueLengthExceedsBuffer, PayloadNoKeyException, PayloadValueLengthException, PayloadValueException, PayloadEmptyException, PacketEmptyException, PayloadIncompleteException, PacketIncompleteException;
}
