package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.Constants;
import com.s73504152.CPEN431.A5.Helper.MessageHelper;
import com.s73504152.CPEN431.A5.Interfaces.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by chbndrhnns on 27.01.2016.
 */
/**
 * Class that represents a Request.
 */
public class Request implements Message {
    private static Logger logger = Logger.getLogger(Request.class.getName());

    private InetAddress sourceAddress;
    private InetAddress destinationAddress;
    private int sourcePort = -1;
    private int destinationPort = -1;

    private byte[] messageId;
    private RequestPayload requestPayload;

    @Override
    public void setDestinationAddress(InetAddress destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    @Override
    public InetAddress getDestinationAddress() {
        return destinationAddress;
    }

    @Override
    public void setDestinationPort(int destinationPort) {
        this.destinationPort = destinationPort;
    }

    @Override
    public int getDestinationPort() {
        return destinationPort;
    }

    @Override
    public void setSourceAddress(InetAddress sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    @Override
    public InetAddress getSourceAddress() {
        return this.sourceAddress;
    }

    @Override
    public void setSourcePort(int sourcePort) {
        this.sourcePort = sourcePort;

    }

    @Override
    public int getSourcePort() {
        return this.sourcePort;
    }


    public void setMessageId(byte[] messageId) {
        this.messageId = messageId;
    }


    public String getMessageIdAsString() {
        if (this.messageId == null) return "";
        return javax.xml.bind.DatatypeConverter.printHexBinary(this.messageId);
    }

    @Override
    public void setPayload(Object payload) {
        this.requestPayload = (RequestPayload) payload;
    }

    public byte[] getMessageId() {
        return this.messageId;
    }

    /*
    constructor, port given so that the message ID can be created
     */
    public Request(Logger logger) {
        Request.logger = logger;
        this.requestPayload = new RequestPayload(logger);
    }

    public RequestPayload getPayload() {
        return requestPayload;
    }

    /*
     process an incoming packet
    */
    @Override
    public void parseData(byte[] raw) throws IOException, PayloadCommandInvalidException, PayloadValueLengthInvalidException,
            PayloadValueLengthExceedsBuffer, PayloadNoKeyException,
            PayloadValueLengthException, PayloadValueException, PayloadEmptyException, PacketEmptyException, PacketIncompleteException {

        if (raw == null) throw new PacketEmptyException();
        if (raw.length < Constants.PACKET_MIN_LEN) throw new PacketIncompleteException();

        // extract message id
        int offset = 0;
        byte [] messageId = new byte[Constants.MSG_ID_LEN];
        System.arraycopy(raw, 0, messageId, 0, Constants.MSG_ID_LEN);
        this.setMessageId(messageId);
        offset += Constants.MSG_ID_LEN;

        // extract payload
        byte[] rawPayload = new byte[raw.length - offset];
        System.arraycopy(raw, offset, rawPayload, 0, raw.length - offset);
        // logger.log(Level.INFO, "Proc: msgId=" + incomingMessageIdString +  ", payload=" + rawRequestPayload.length + " bytes");
        this.requestPayload.parseByteArray(rawPayload, this.getMessageId());
    }

    /*
    Returns a ByteArray with message ID and payload.
     */
    @Override
    public byte[] getPacketBytes() throws IOException, PayloadIncompleteException, PayloadValueException, PayloadEmptyException, PayloadValueLengthException {
        if (messageId == null) {
            this.messageId = MessageHelper.generateMessageId(this.sourcePort);
            logger.log(Level.INFO, "MessageId missing. Using msgId=" + getMessageIdAsString());
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(this.messageId);
        outputStream.write(requestPayload.getPayloadBytes());

        return outputStream.toByteArray();
    }

}
