package com.s73504152.CPEN431.A5.Helper;

import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Class that provides a nice format for log messages.
 */
// taken from http://stackoverflow.com/questions/194765/how-do-i-get-java-logging-output-to-appear-on-a-single-line
public class SingleLineFormatter extends Formatter {
    //
    // Create a DateFormat to format the logger timestamp.
    //
    // private static final DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS");
    private static final DateFormat df = new SimpleDateFormat("hh:mm:ss.SSS");
    private static InetAddress host = null;

    public SingleLineFormatter(InetAddress address) {
        host = address;
    }

    public String format(LogRecord record) {
        StringBuilder builder = new StringBuilder(1000);
        builder.append("[").append(host.getHostName()).append("] - ");
        builder.append(df.format(new Date(record.getMillis()))).append(" - ");
        builder.append("[#").append(String.format("%03d", record.getThreadID())).append("] - ");
        // builder.append("[").append(record.getLevel()).append("] - ");
        builder.append(formatMessage(record));
        builder.append(" [").append(record.getSourceMethodName()).append("]");
        // builder.append(record.getSourceMethodName()).append("] - ");
        builder.append("\n");
        return builder.toString();
    }

    public String getHead(Handler h) {
        return super.getHead(h);
    }

    public String getTail(Handler h) {
        return super.getTail(h);
    }
}