package com.s73504152.CPEN431.A5.Helper;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import javax.xml.bind.DatatypeConverter;

/**
 * Class that contains helper functions
 */
public class ByteArrayHelper {

    /*
    Converts an integer getValue into a ByteArray.
     */
    public static byte[] intToByteArray(int value) {
        BigInteger bigInt = BigInteger.valueOf(value);
        return bigInt.toByteArray();
    }


/*
 * Converts an integer value to a 4 byte byte array
  */
    public static byte[] intTo4ByteArray( int i ) {
        byte[] result = new byte[4];
        result[0] = (byte) (i >> 24);
        result[1] = (byte) (i >> 16);
        result[2] = (byte) (i >> 8);
        result[3] = (byte) (i /*>> 0*/);

        return result;
    }

    /*
    Converts a ByteArray to an integer getValue.
     */
    public static int byteArrayToInt(byte [] bytes) {
        return ByteBuffer.wrap(bytes).getInt();
    }

    /*
    Converts a short getValue into a ByteArray.
     */
    public static byte[] shortToByteArray(short x) {
        byte[] ret = new byte[2];
        ret[0] = (byte)(x & 0xff);
        ret[1] = (byte)((x >> 8) & 0xff);
        return ret;
    }

    /*
    Converts a ByteArray to a short getValue.
     */
    public static short byteArrayToShort(byte [] bytes) {
        return ByteBuffer.wrap(bytes).getShort();
    }

    /*
    Converts a ByteArray into a String.
     */
    public static String byteArrayToString(byte[] array) {
        return DatatypeConverter.printHexBinary(array);
    }

    /*
    Converts a String into a ByteArray.
     */
    public static byte[] stringToByteArray(String s) {
        return DatatypeConverter.parseHexBinary(s);
    }

    /*
    Converts ByteArray to Hex String
    From docs.oracle
    https://docs.oracle.com/javase/7/docs/api/javax/xml/bind/DatatypeConverter.html
     */
    public static String byteArrayToHex(byte[] array) {
        return DatatypeConverter.printHexBinary(array);
    }

    public static byte[] intTo2ByteArray(int srcPort) {
        byte[] data = new byte[2];
        data[0] = (byte) srcPort;
        data[1] = (byte) (srcPort >>> 8);
        return data;
    }
}
