package com.s73504152.CPEN431.A5.Helper;

import com.s73504152.CPEN431.A5.*;
import com.s73504152.CPEN431.A5.Exceptions.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.*;

/**
 * Created by Simon on 14-03-2016.
 */
public class KVSTest {
    RequestReplyProtocol rrp;
    String hostname;
    int port;
    int timeout;
    boolean retry;
    int valueLength;
    public static final Logger logger = Logger.getLogger(KVSTest.class.getName());
    private long sleep = 2000;

    public KVSTest(String hostname, int valueLength) {
        setupLogger();
        this.hostname = hostname;
        this.valueLength = valueLength;

        logger.log(Level.INFO, "Using host: " + hostname + ", getValue length: " + valueLength);

        rrp = new RequestReplyProtocol(logger);
        timeout = 200;
        retry = true;
    }

    public void test2048() {

        port = 14152;

        byte[] key = MessageHelper.generateRandomKey();
        int rounds = 1;

        while (true) {
            sendMessage(COMMAND.PUT, key, valueLength);

            sendMessage(COMMAND.GET, key, valueLength);

            sendMessage(COMMAND.REMOVE, key, valueLength);

            sendMessage(COMMAND.REMOVE, key, valueLength);

            sendMessage(COMMAND.GET, key, valueLength);

            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            logger.log(Level.INFO, "Rounds: " + rounds);
            rounds++;
        }

    }

    public void test10000() {

        hostname = "montana..";
        port = 14152;

        int valueLength = 10000;
        byte[] key = MessageHelper.generateRandomKey();

        sendMessage(COMMAND.PUT, key, valueLength);

        sendMessage(COMMAND.GET, key, valueLength);

        sendMessage(COMMAND.REMOVE, key, valueLength);

        sendMessage(COMMAND.REMOVE, key, valueLength);

        sendMessage(COMMAND.GET, key, valueLength);

    }

    private void sendMessage(COMMAND command, byte[] key, int valueLength) {
        Request request = new Request(logger);
        RequestPayload requestPayload = null;
        try {
            request.setDestinationAddress(InetAddress.getByName(hostname));
            request.setDestinationPort(port);
            requestPayload = new RequestPayload(logger);
            requestPayload.setCommand(command);
            requestPayload.setKey(key);
            requestPayload.setValue(MessageHelper.generateRandomValue(valueLength));
            request.setPayload(requestPayload);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (KeyLenMismatchException e) {
            e.printStackTrace();
        } catch (PayloadValueTooLongException e) {
            e.printStackTrace();
        }

        RequestReplyProtocol requestReplyProtocol = new RequestReplyProtocol(logger);
        try {
            Response response = requestReplyProtocol.sendAndReceive(request, timeout, retry);
            ResponsePayload responsePayload = response.getPayload();
            logger.log(Level.INFO, command.toString() + " [" + valueLength + "], err: " + responsePayload.getErrorCode().toString());
        } catch (RequestTimeoutException e) {
            System.out.println("TIMEOUT");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        String hostname;
        int valueLength = 2048;
        if (args.length == 0){
            hostname = "planetlab2.cs.ubc.ca";
        } else {
            try {
                new ServerApp(InetAddress.getByName("169.254.119.142"), 14152, true, false, false, true);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            hostname = args[0];
        }
        if (args.length == 2) valueLength = Integer.parseInt(args[1]);

        new KVSTest(hostname, valueLength).test2048();
    }

    /*
setup custom Formatter for logging
*/
    public void setupLogger()  {
        logger.setUseParentHandlers(false);
        if (logger.getHandlers().length == 0) {
            try {
                Handler cons = new ConsoleHandler();
                cons.setLevel(Constants.LOGGER_LEVEL);
                cons.setFormatter(new SingleLineFormatter(InetAddress.getByName(hostname)));
                logger.addHandler(cons);

                Handler file = new FileHandler("logs/myServer.%u.%g.log", Constants.LOGGER_MAX_SIZE_PER_LOGFILE, Constants.LOGGER_MAX_NR_OF_LOGFILES, true);
                file.setLevel(Constants.LOGGER_LEVEL);
                file.setFormatter(new SingleLineFormatter(InetAddress.getByName(hostname)));
                logger.addHandler(file);
            }
            catch (IOException e) {
                System.out.println("ERR: Could not initialize log file.");
            }
        }
    }
}
