package com.s73504152.CPEN431.A5.Helper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Random;

/**
 * Created by Johannes on 07.04.2016.
 */
public class MessageHelper {

    public static byte[] generateMessageId(int srcPort) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            Random random = new Random();
            short randomNumber = (short) (Math.abs(random.nextInt()));

            outputStream.write(InetAddress.getLocalHost().getAddress());
            outputStream.write(ByteArrayHelper.intTo2ByteArray(srcPort));
            outputStream.write(ByteBuffer.allocate(Short.SIZE / Byte.SIZE).putShort(randomNumber).array());
            outputStream.write(ByteBuffer.allocate(Long.SIZE / Byte.SIZE).putLong(System.currentTimeMillis()).array());

        } catch (UnknownHostException e) {
            System.out.println("Error getting local address.");
        }
        catch (NullPointerException e) {
            System.out.println("Error setting some part of messageId.");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return outputStream.toByteArray();
    }

    public static byte[] generateRandomKey() {
        byte[] key = new byte[32];
        new Random().nextBytes(key);
        return key;
    }

    public static byte[] generateRandomValue(int len) {
        byte[] value = new byte[len];
        new Random().nextBytes(value);
        return value;
    }
}
