package com.s73504152.CPEN431.A5.Helper;

import com.s73504152.CPEN431.A5.*;
import com.s73504152.CPEN431.A5.Exceptions.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;

/**
 * Created by Simon on 04-03-2016.
 * Code inspired by guide from:
 * http://docs.oracle.com/javase/tutorial/networking/datagrams/clientServer.html
 */

public class CommandLineClient {
    public static final Logger logger = Logger.getLogger(CommandLineClient.class.getName());

    private static byte[] payload;
    private static InetAddress address;
    private static int port;
    private static byte operation;
    private static String key;
    private static String value;
    private static int initialTimeout = 200;
    private static boolean retry = true;


    public static void main(String[] args) {

        readArguments(args);

        Request request = new Request(logger);
        RequestPayload requestPayload = null;
        try {
            requestPayload = new RequestPayload(logger);
            requestPayload.setCommand(COMMAND.valueOf(operation));
            requestPayload.setKey(ByteArrayHelper.stringToByteArray(key));
            requestPayload.setValue(ByteArrayHelper.stringToByteArray(value));
            request.setPayload(requestPayload);
        } catch (KeyLenMismatchException | PayloadValueTooLongException e) {
            e.printStackTrace();
        }


        RequestReplyProtocol requestReplyProtocol = new RequestReplyProtocol(logger);
        try {
            Response response = requestReplyProtocol.sendAndReceive(request, initialTimeout, retry);
            ResponsePayload responsePayload = response.getPayload();
            System.out.println("Reply length: " + responsePayload.getPayloadBytes().length);
            System.out.println("Error code (hex): " + responsePayload.getErrorCode().toString());
            System.out.println("Key:          " + key);
            System.out.println("Value length: " + requestPayload.getValueLength());
            System.out.println("Value (hex):  " + value);

            //System.out.println("Full request: " + ByteArrayHelper.byteArrayToHex(request));
            //printByteArray(request);
            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    private static void printByteArray(byte[] buf) {
        for (byte aBuf : buf) {
            System.out.print(aBuf + " ");
        }
        System.out.println();
    }

    private static void readArguments(String[] args) {
        if (args.length == 7 || args.length == 6) {
            switch (args[4]) {
                case "put":
                    operation = (byte) 1;
                    System.out.println("Put operation");
                    break;
                case "get":
                    operation = (byte) 2;
                    System.out.println("Get operation");
                    break;
                case "remove":
                    operation = (byte) 3;
                    System.out.println("Remove operation");
                    break;
                case "shutdown":
                    operation = (byte) 4;
                    System.out.println("Shutdown operation");
                    break;
                case "deleteall":
                    operation = (byte) 5;
                    System.out.println("Delete-all operation");
                    break;
            }
            try {
                address = InetAddress.getByName(args[0]);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            port = Integer.parseInt(args[1]);
            retry = Boolean.parseBoolean(args[2]);
            initialTimeout = Integer.parseInt(args[3]);
            key = args[5];
            value = args[6];


        } else {
            System.out.println("Wrong number of arguments \n"
                    + "Usage:  java com.s65011158.cpen431.A2.A2Client <server address> <server port> <retry=true|false> <initialTimeout> <command> <key> <getValue> \n"
                    + "<command> = {put, get, remove, deleteall, shutdown}");
            System.exit(0);
        }

    }

}
