package com.s73504152.CPEN431.A5.Helper;

import com.s73504152.CPEN431.A5.*;

import java.net.BindException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by chbndrhnns on 03.03.2016.
 */
public class PacketReceiver implements Runnable {
    private final ReplicationManager replicationManager;
    private final ConsistentHashing serverKeySpace;
    private final Logger logger;
    private final ServerApp serverApp;
    private boolean externalListener = false;
    private final Cache serverCache;
    private final Storage serverStorage;

    private final InetAddress serverAddress;
    private final int serverPort;

    public PacketReceiver(ServerApp serverApp, InetAddress serverAddress, int serverPort, boolean externalListener, Storage serverStorage, Cache serverCache, ConsistentHashing keySpace, ReplicationManager replicationManager, Logger logger) {
        this.logger = logger;
        this.serverApp = serverApp;
        this.serverPort = serverPort;
        this.serverAddress = serverAddress;
        this.externalListener = externalListener;
        this.serverCache = serverCache;
        this.serverStorage = serverStorage;
        this.replicationManager = replicationManager;
        this.serverKeySpace = keySpace;
    }

    @Override
    public void run() {
        ExecutorService pool = Executors.newFixedThreadPool(Constants.SERVER_THREADS);

        try (DatagramSocket serverSocket = new DatagramSocket(serverPort, serverAddress)) {
            String typeOfListener = "external";
            if (!externalListener) typeOfListener = "internal";
            logger.log(Level.INFO, "Listening on ip=" + this.serverAddress.toString() + ", port=" + serverPort + " (" + typeOfListener + ")");

            //noinspection InfiniteLoopStatement
            while (true) {
                byte[] buffer = new byte[Constants.PACKET_MAX_LEN];
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                serverSocket.receive(packet);
                try {
                    pool.execute(new ServerPacketHandler(packet, serverAddress, serverPort, externalListener, serverStorage, serverCache, serverKeySpace, replicationManager, logger, serverApp));
                }
                catch (NegativeArraySizeException e) {
                    // invalid data sent - ignore this, continue
                }
            }
        }
        catch (BindException e) {
            logger.log(Level.INFO, "Could not bind to port " + serverPort);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
