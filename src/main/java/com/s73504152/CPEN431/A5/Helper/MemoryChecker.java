package com.s73504152.CPEN431.A5.Helper;

import com.s73504152.CPEN431.A5.Exceptions.OutOfHeapSpaceException;
import com.s73504152.CPEN431.A5.ServerApp;
import com.s73504152.CPEN431.A5.Storage;
import org.github.jamm.MemoryMeter;

import java.util.logging.*;

/**
 * Created by Johannes on 27.01.2016.
 * based upon tutorial from http://crunchify.com/implement-simple-threadsafe-cache-using-hashmap-without-using-synchronized-collection/
 */
public class MemoryChecker {
    public static Logger logger;
    private long interval;
    private Storage dataStorage;
    private MemoryMeter meter;
    public long maxMem;

    public MemoryChecker(Storage dataStorage, final Logger logger) {
        logger.log(Level.INFO, "MemoryChecker for Storage Class");
        MemoryChecker.logger = logger;
        this.dataStorage = dataStorage;
        this.maxMem = Runtime.getRuntime().maxMemory();
    }

    public MemoryChecker(Storage dataStorage, final Logger logger, final long interval) {
        MemoryChecker.logger = logger;
        this.dataStorage = dataStorage;
        this.maxMem = Runtime.getRuntime().maxMemory();


        try {
            meter = new MemoryMeter();
        }
        catch (NoClassDefFoundError e) {
            // fail silently
        }

        if (interval > 0) {
            Thread t = new Thread(new Runnable() {
                public void run() {
                    while(true) {
                        try {
                            Thread.sleep(interval * 1000);
                            // logger.log(Level.INFO, "Storage=" + meter.measureDeep(ServerApp.dataStorage));
                            // logger.log(Level.INFO, "Cache=" + meter.measureDeep(ServerApp.cache));
                            Long kvsSize = Storage.byteCount;
                            long freeMem = Runtime.getRuntime().freeMemory();
                            if (freeMem < maxMem * 0.1) {
                                // if (ServerApp.debug) logger.log(Level.INFO, "gc, kvs=" + String.valueOf(kvsSize) + " B, free=" + String.valueOf(freeMem) + " B");
                                // System.gc();
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        catch (OutOfMemoryError | NoClassDefFoundError e) {
                            // comes from the garbage collector if it is inefficient, ignore
                        }
                    }
                }
            });

            t.setDaemon(true);
            t.start();
        }
    }

    /*
    check for available heap space and decline to put another key in case of too few available space
    */
    public void checkServerResources() throws OutOfHeapSpaceException {
        Long kvsSize = Storage.byteCount;
        long freeMem = Runtime.getRuntime().freeMemory();

        if (ServerApp.debug) {
            // logger.log(Level.INFO, "free=" + String.valueOf(freeMem) + ", kvs=" + String.valueOf(kvsSize));
        }

        if (kvsSize > maxMem * Constants.SERVER_MAX_KVS_SIZE && freeMem < Constants.SERVER_HEAP_THRESHOLD) {
            logger.log(Level.INFO, "outofmem: low mem; kvs=" + String.valueOf(kvsSize) + " B, mem=" + String.valueOf(freeMem));
            throw new OutOfHeapSpaceException();
        }

//        if (freeMem > Constants.SERVER_HEAP_THRESHOLD) {
//            if (kvsSize > maxMem * Constants.SERVER_MAX_KVS_SIZE) {
//                logger.log(Level.INFO, "outofmem: large kvs; kvs=" + String.valueOf(kvsSize) + " B, mem=" + String.valueOf(freeMem) + "B");
//                throw new OutOfHeapSpaceException();
//            }
//        }
//        else {
//            Runtime.getRuntime().gc();
//            logger.log(Level.INFO, "outofmem: low mem; kvs=" + String.valueOf(kvsSize) + ", mem=" + String.valueOf(freeMem));
//            throw new OutOfHeapSpaceException();
//        }
    }

}
