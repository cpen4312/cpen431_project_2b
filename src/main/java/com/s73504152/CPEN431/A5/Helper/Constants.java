package com.s73504152.CPEN431.A5.Helper;

import java.util.logging.Level;

/**
 * Class that contains constant values for use in the other classes.
 */
public final class Constants {
    public static final String SERVER_NODES_STAGING = "nodes.txt";
    public static final String SERVER_NODES_TEST = "nodes_local.txt";
    public static final int SERVER_PUBLIC_PORT = 14152;
    public static final int SERVER_INTERNAL_PORT_OFFSET = 1000;
    public static final int SERVER_RECEIVE_TIMEOUT = 1000;
    public static final int SERVER_THREADS = 4;
    public static final long SERVER_HEAP_THRESHOLD = 5000000;
    public static final int SERVER_MEM_CLEANUP_CYLCE = 10;
    // size of the kvs in percent of the total available mem
    public static final double SERVER_MAX_KVS_SIZE = 0.75;
    public static final int STORAGE_MAX_NR_OF_OBJECTS = 100000;

    // cache
    public static final int CACHE_TIME_TO_LIVE = 5;
    public static final int CACHE_CLEANUP_CYCLE = 1;

    // application protocol specific
    public static final int CMD_LEN = 1;
    public static final int KEY_LEN = 32;
    public static final int VAL_LEN_LEN = 2;
    public static final int VAL_MAX_LEN = 10000;

    // request/response protocol specific
    public static final int MSG_ID_LEN = 16;
    public static final int PAYLOAD_MAX_LEN = CMD_LEN + KEY_LEN + VAL_LEN_LEN + VAL_MAX_LEN;
    public static final int PAYLOAD_MIN_LEN = CMD_LEN;
    public static final int PACKET_MAX_LEN = PAYLOAD_MAX_LEN + MSG_ID_LEN;
    public static final int PACKET_MIN_LEN = MSG_ID_LEN + CMD_LEN;

    // Testing application
    public static final String TEST_SERVER_ADDRESS = "127.0.0.1";

    // Logger-specific
    public static final int LOGGER_PRINT_MAX_VAL_LEN = 15;
    public static final int LOGGER_PRINT_MAX_PAYLOAD_LEN = 50;
    public static final Level LOGGER_LEVEL = Level.INFO;
    public static final int LOGGER_MAX_SIZE_PER_LOGFILE = 5000000;
    public static final int LOGGER_MAX_NR_OF_LOGFILES = 20;
    public static final int SERVER_INTERNAL_MAX_SEND_RETRY = 4;

    // gossip-specific
    public static final int GOSSIP_PORT_OFFSET = 2000;
    public static final int GOSSIP_INTERVAL = 500;
    public static final int GOSSIP_CLEANUP_INTERVAL = 5000;

    // Consistent Hashing
    public static final int KEY_LENGTH_BYTES = 4;
    public static final int NUMBER_OF_VIRTUAL_NODES = 5;
    public static final int REPLICATION_FACTOR = 3;

    //UBC node
    public static final String UBC_NODE = "planetlab2.cs.ubc.ca";

}
