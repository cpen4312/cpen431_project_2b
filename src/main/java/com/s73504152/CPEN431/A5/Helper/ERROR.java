package com.s73504152.CPEN431.A5.Helper;

/*
enum that holds all the possible error codes
 */
public enum ERROR {
    OK(0),
    KEY_NOT_FOUND(1),
    OUT_OF_MEM(2),
    OVERLOAD(3),
    INTERNAL(4),
    CMD_INVALID(5),
    VAL_LEN_INVALID(6),
    VAL_LEN_EXCEEDS_BUFFER(0x21),
    REQUEST_INCOMPLETE(0x22);

    private int id;

    ERROR(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static ERROR valueOf(int id) {
        for (ERROR cmd : values()) {
            if (cmd.id == id) {
                return cmd;
            }
        }
        return null;
    }
}
