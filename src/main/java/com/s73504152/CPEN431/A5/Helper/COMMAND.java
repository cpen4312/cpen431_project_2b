package com.s73504152.CPEN431.A5.Helper;

/*
    enum that holds the possible commands
*/
public enum COMMAND {
    INVALID(-1),
    PUT(1),
    GET(2),
    REMOVE(3),
    SHUTDOWN(4),
    DELETE_ALL(5),
    IS_ALIVE(6),
    GET_PID(7),
    PUT_REPLICA(21),
    REMOVE_REPLICA(23),
    MERGE_LISTS(24),
    IS_ALIVE_OLD(99);

    private int id;

    COMMAND(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static COMMAND valueOf(int id) {
        for (COMMAND cmd : values()) {
            if (cmd.id == id) {
                return cmd;
            }
        }
        return null;
    }
}