package com.s73504152.CPEN431.A5.Helper;

import java.io.*;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by duncan on 2016-04-04.
 */
public class HashMapSerializer {


    public static byte[] packageMap(HashMap<String, Integer> livingList, Logger logger) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out;
        byte[] mapArray;
        try{
            out = new ObjectOutputStream(bos);
            out.writeObject(livingList);
            mapArray = bos.toByteArray();
            out.close();
            bos.close();
            return mapArray;

        }catch(IOException e) {
            logger.log(Level.INFO, "Err: IO Exception: " + e.getMessage());
        }
        return null;
    }

    public static HashMap<String, Integer> unpackageMap(byte[] byteMap, Logger logger){
        ByteArrayInputStream bis = new ByteArrayInputStream(byteMap);
        ObjectInput in = null;
        try{
            in = new ObjectInputStream(bis);
            HashMap<String, Integer> inMap = (HashMap<String, Integer>) in.readObject();
            return inMap;
        } catch (IOException e) {
            logger.log(Level.INFO, "Err: IO Exception: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            logger.log(Level.INFO, "Err: Class not found exception: " + e.getMessage());
        }
        return null;
    }

    public static HashMap<String, Integer> addKeys(HashMap<String, Integer> from, HashMap<String, Integer> to){
        Integer value = 0;
        for (String key : from.keySet()){
            value = from.get(key);
            if(to.containsKey(key)){
                value += to.get(key);
            }
            to.put(key, value);
        }
        return to;
    }


}
