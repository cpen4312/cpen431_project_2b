package com.s73504152.CPEN431.A5;

import java.util.*;
import java.util.logging.Logger;

/**
 * Created by chbndrhnns on 27.01.2016.
 * based upon tutorial from http://crunchify.com/implement-simple-threadsafe-cache-using-hashmap-without-using-synchronized-collection/
 */
public class Cache {

    private long ttl;
    private final HashMap<String, CachedObject> cache;

    public Cache(long timeToLive, final long interval, Logger logger) {
        Logger logger1 = logger;
        this.ttl = timeToLive;
        cache = new HashMap<>();

        if (ttl > 0 && interval > 0) {
            Thread t = new Thread(() -> {
                while(true) {
                    try {
                        Thread.sleep(interval * 1000);
                        clean();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });

            t.setDaemon(true);
            t.start();
        }
    }

    public void put(String key, CachedObject value) {
        synchronized (cache) {
            cache.put(key, value);
        }
    }

    public CachedObject get(String k) {
        // printKeys();
        synchronized (cache) {
            CachedObject o = cache.get(k);
            long  now = System.currentTimeMillis();
            if (o != null && now > (o.timestamp - ttl * 1000)) {
                // System.out.println("ttl ok for key=" + k);
                return o;
            }
            // System.out.println("ttl expired for key=" + k);
            return null;
        }
    }

    public int size() {
        synchronized (cache) {
            return cache.size();
        }
    }

    public void printKeys() {
        System.out.println("Cache size: " + cache.size());
        synchronized (cache) {
            Iterator<?> itr = cache.keySet().iterator();

            while (itr.hasNext()) {
                String key = (String) itr.next();
                System.out.println((key));
            }
        }
    }

    public void clean() {
        long now = System.currentTimeMillis();
        ArrayList<String> deleteKey;

        synchronized (cache) {
            if (ServerApp.debug && cache.size() > 0) {
                // logger.log(Level.INFO, "Cache size before cleanup: " + cache.size());
            }
            Iterator<?> itr = cache.keySet().iterator();

            deleteKey = new ArrayList<>((cache.size() / 2) + 1);
            CachedObject c;

            while (itr.hasNext()) {
                String key = (String) itr.next();
                c = cache.get(key);
                if (c != null && (now > (ttl * 1000 + c.timestamp))) {
                    deleteKey.add(key);
                }
            }
        }

        for (String key : deleteKey) {
            synchronized (cache) {
                cache.remove(key);
            }

            Thread.yield();
        }

//        if (ServerApp.debug && cache.size() > 0) {
//            logger.log(Level.INFO, "Cache size after cleanup: " + cache.size());
//        }
    }
}
