package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.COMMAND;
import com.s73504152.CPEN431.A5.Helper.Constants;
import com.s73504152.CPEN431.A5.Helper.HashMapSerializer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by duncan on 2016-04-03.
 */
public class NodeDecider {

    //this class is going to handle the isAlive threads that will be created. one is going to start and pass a list of good nodes to the other.

    private ConsistentHashing keySpace;
    private Logger logger;
    private InetAddress address;
    private RequestReplyProtocol requestForwarder;
    private ResponsePayload responsePayload;

    //NodeStateDiscover
    public NodeStateDiscover nodeState;

    /**
     * Constructor
     * @param keySpace
     * @param logger
     * @param address
     */
    public NodeDecider(ConsistentHashing keySpace, Logger logger, InetAddress address) {
        this.keySpace = keySpace;
        this.logger = logger;
        this.address = address;
        this.requestForwarder = new RequestReplyProtocol(logger);
        this.responsePayload = new ResponsePayload(logger);
    }

    public void nodeDiscover() throws UnknownHostException{
        String hostName = nodeState.getHostName().getHostName();
        InetAddress ubc = InetAddress.getByName(Constants.UBC_NODE);

        String[] allNodes = keySpace.getAllNodes();
        HashMap<String, Integer> livingList = nodeState.getLivingList();

        //Start web of nodes.
        try {
            this.nodeState = new NodeStateDiscover(keySpace, logger, address);
            (new Thread(this.nodeState)).start();
        } catch (Exception e){
            e.printStackTrace();
        }
        //hostname != ubc then send hashmap to ubc
        if(hostName != ubc.getHostName()){

            RequestPayload requestPayload = null;
            byte[] mapArray = HashMapSerializer.packageMap(livingList, logger);

            //build requestPayload
            requestPayload = new RequestPayload(logger);

            try {
                if (ServerApp.debug)
                    logger.log(Level.INFO, "Sending Hashmap from '" + hostName + "', port=" + this.keySpace.getPort(hostName));
                requestPayload.setCommand(COMMAND.MERGE_LISTS);
                requestPayload.setValue(mapArray);

                //build request
                Request request = new Request(logger);
                request.setDestinationAddress(ubc);
                request.setDestinationPort(keySpace.getPort(Constants.UBC_NODE) + Constants.SERVER_INTERNAL_PORT_OFFSET);
                request.setPayload(requestPayload);

                Response response = requestForwarder.sendAndReceive(request, Constants.SERVER_RECEIVE_TIMEOUT, true);
                responsePayload = response.getPayload();
            } catch (IOException e) {
                logger.log(Level.INFO, "ERR: IO Exception: " + e.getMessage());
            } catch (RequestTimeoutException e) {
                logger.log(Level.INFO, "ERR: Request timeout exception: " + e.getMessage());
            } catch (PayloadValueTooLongException e) {
                logger.log(Level.INFO, "ERR: Payload too long exception: " + e.getMessage());
            }
        }else{

            //create a command, for merging hashmaps and listening. use the same logic and RequestReplyProtocol to do this.
            // make a master map which is merged with all other maps.



        }
            //else (hostname == ubc) do logic with lists.

    }



}
