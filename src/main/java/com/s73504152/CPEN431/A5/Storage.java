package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.ByteArrayHelper;
import com.s73504152.CPEN431.A5.Helper.Constants;
import com.s73504152.CPEN431.A5.Helper.MemoryChecker;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.*;

/**
 * Created by chbndrhnns on 27.01.2016.
 */
public class Storage {
    public static Logger logger;
    public static long byteCount;
    private MemoryChecker memory;

    private Map<ByteBuffer, byte[]> dataStorage = null;

    /**
     * Storage constructor
     */
    public Storage(Logger logger) {
        Storage.logger = logger;
        this.dataStorage = new ConcurrentHashMap<>(4000, 0.9f, 1);
        this.memory = new MemoryChecker(this, logger);
        logger.log(Level.INFO, "New k/v store created");
        byteCount = 0L;
    }

    /*
    retrieve the storage object
    */
    public Map getStorageObject() {
        return this.dataStorage;
    }

    /*
    return number of elements in the kv store
     */
    public int getSize() {
        return this.dataStorage.size();
    }

    /*
        get a KV value by key
    */
    public byte[] get(byte[] key) throws KeyLenMismatchException, KeyNotFoundException {
        byte[] value;

        if (!this.checkKeyLength(key)) {
            throw new KeyLenMismatchException();
        }

        value = this.dataStorage.get(ByteBuffer.wrap(key));
        if (null == value) throw new KeyNotFoundException();

        return value;
    }

    public Set<ByteBuffer> getKeySet(){
        return dataStorage.keySet();
    }

    /*
    put a KV pair into the store
    */
    public void put(byte[] key, byte[] value) throws KeyLenMismatchException, StorageFullException, OutOfHeapSpaceException {
        if (!checkKeyLength(key)) throw new KeyLenMismatchException();
        memory.checkServerResources();

        if (this.dataStorage.size() < Constants.STORAGE_MAX_NR_OF_OBJECTS) {
            this.dataStorage.put(ByteBuffer.wrap(key), value);
            Storage.byteCount = Storage.byteCount + (long) value.length + (long) Constants.KEY_LEN;
        }
        else
        {
            throw new StorageFullException();
        }

    }

    /*
    check if the key has the required length
    */
    private boolean checkKeyLength(byte[] key)  {
        return key.length == Constants.KEY_LEN;
    }

    /*
    remove a KV pair by key
    */
    public void remove(byte[] key) throws KeyNotFoundException, OutOfHeapSpaceException {
        if (!this.dataStorage.containsKey(ByteBuffer.wrap(key))) throw new KeyNotFoundException();

        try {
            int len = get(key).length;
            this.dataStorage.remove(ByteBuffer.wrap(key));
            Storage.byteCount = Storage.byteCount - (long) len - (long) Constants.KEY_LEN;
        }
        catch (Exception e) {
            logger.log(Level.INFO, "Remove failed for key=" + ByteArrayHelper.byteArrayToString(key));
        }
    }

    /*
    remove all keys from the hash map
    */
    public void removeAll() throws StorageEmptyException, KeyNotFoundException, OutOfHeapSpaceException {
        if (this.dataStorage.size() == 0) throw new StorageEmptyException();

        try {
            for (ByteBuffer key : this.dataStorage.keySet()) {
                this.dataStorage.remove(key);
            }
            Storage.byteCount = 0;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        memory.checkServerResources();
    }
}
