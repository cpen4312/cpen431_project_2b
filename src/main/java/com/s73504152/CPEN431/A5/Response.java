package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.Constants;
import com.s73504152.CPEN431.A5.Helper.MessageHelper;
import com.s73504152.CPEN431.A5.Interfaces.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by chbndrhnns on 27.01.2016.
 */
/**
 * Class that holds the Response packet. Payload can be attached.
 */
public class Response implements Message {
    private static Logger logger;

    private byte[] messageId;
    private ResponsePayload responsePayload;

    private InetAddress destinationAddress = null;
    private InetAddress sourceAddress = null;
    private int destinationPort = -1;
    private int sourcePort = -1;

    public void setMessageId(byte[] messageId) {
        this.messageId = messageId;
    }

    @Override
    public String getMessageIdAsString() {
        if (this.messageId == null) return "";
        return javax.xml.bind.DatatypeConverter.printHexBinary(this.messageId);
    }

    @Override
    public void setPayload(Object payload) {
        this.responsePayload = (ResponsePayload) payload;
    }

    @Override
    public void setDestinationAddress(InetAddress destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    @Override
    public InetAddress getDestinationAddress() {
        return destinationAddress;
    }

    @Override
    public void setDestinationPort(int destinationPort) {
        this.destinationPort = destinationPort;

    }

    @Override
    public int getDestinationPort() {
        return this.destinationPort;
    }

    @Override
    public void setSourceAddress(InetAddress sourceAddress) {
        this.sourceAddress = sourceAddress;
    }


    @Override
    public InetAddress getSourceAddress() {
        return this.sourceAddress;
    }

    @Override
    public void setSourcePort(int sourcePort) {
        this.sourcePort = sourcePort;
    }

    @Override
    public int getSourcePort() {
        return this.sourcePort;
    }

    @Override
    public byte[] getMessageId() {
        return this.messageId;
    }

    @Override
    public ResponsePayload getPayload() {
        return responsePayload;
    }


    /*
    * create empty Response object
     */
    public Response(Logger logger) {
        Response.logger = logger;
        this.responsePayload = new ResponsePayload(logger);
    }


    /*
    Parse a given response packet and extract useful data.
     */
    @Override
    public void parseData(byte[] raw) throws PayloadValueLengthExceedsBuffer, PayloadEmptyException, PayloadValueLengthInvalidException, PayloadCommandInvalidException, PayloadValueLengthException, PayloadValueException, PayloadNoKeyException, PacketEmptyException, PayloadIncompleteException, PacketIncompleteException {

        if (raw == null) throw new PacketEmptyException();
        if (raw.length < Constants.PACKET_MIN_LEN) throw new PacketIncompleteException();

        // extract message id
        int offset = 0;
        byte [] messageId = new byte[Constants.MSG_ID_LEN];
        System.arraycopy(raw, 0, messageId, 0, Constants.MSG_ID_LEN);
        this.setMessageId(messageId);
        offset += Constants.MSG_ID_LEN;

        // extract payload
        byte[] rawPayload = new byte[raw.length - offset];
        System.arraycopy(raw, offset, rawPayload, 0, raw.length - offset);
        this.responsePayload.parseByteArray(rawPayload, this.getMessageId());
    }

    /*
   Returns a ByteArray with message ID and payload.
    */
    public byte[] getPacketBytes() throws IOException, PayloadIncompleteException, PayloadEmptyException, PayloadValueException, PayloadCommandMissingException {
        if (messageId == null) {
            this.messageId = MessageHelper.generateMessageId(this.sourcePort);
            logger.log(Level.INFO, "MessageId missing. Using msgId=" + getMessageIdAsString());
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(messageId);
        outputStream.write(responsePayload.getPayloadBytes());

        return outputStream.toByteArray();
    }
}
