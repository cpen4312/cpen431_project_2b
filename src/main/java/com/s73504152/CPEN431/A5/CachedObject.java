package com.s73504152.CPEN431.A5;

/**
 * Created by chbndrhnns on 27.01.2016.
 */
public class CachedObject {
    private final ResponsePayload responsePayload;
    public long timestamp = System.currentTimeMillis();
    private boolean processed = false;

    public ResponsePayload getResponsePayload() {
        return responsePayload;
    }

    public boolean isProcessed() {
        return processed;
    }
    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public CachedObject(ResponsePayload payload) {
        this.responsePayload = payload;
    }

}
