package com.s73504152.CPEN431.A5;

import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.COMMAND;
import com.s73504152.CPEN431.A5.Helper.Constants;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.s73504152.CPEN431.A5.Helper.ERROR.CMD_INVALID;

/**
 * Created by Duncan Brown Esquire on 2016-03-29.
 */
public class NodeStateDiscover implements Runnable {

    private RequestReplyProtocol aliveQueryForwarder;
    private ConsistentHashing serverKeySpace;
    private Logger logger;
    private final InetAddress serverAddress;
    private ResponsePayload responsePayload;
    /**
     * Map of all nodes, counter indicating number of times it has failed the is alive tests
     */
    private HashMap<String, Integer> livingList = new HashMap<>();
    private String[] nodeList;


    /**
     * Constructor:
     *
     * @param serverKeySpace
     * @param logger
     * @param serverAddress
     */
    public NodeStateDiscover(ConsistentHashing serverKeySpace, Logger logger, InetAddress serverAddress) {
        this.aliveQueryForwarder = new RequestReplyProtocol(logger);
        this.serverKeySpace = serverKeySpace;
        this.logger = logger;
        this.serverAddress = serverAddress;
        this.responsePayload = new ResponsePayload(logger);
        nodeList = serverKeySpace.getAllNodes();
        for (int i = 0; i < nodeList.length; i++) {
            this.livingList.put(nodeList[i], -1);
        }
    }

    public InetAddress getHostName() {
        return this.serverAddress;
    }

    public HashMap<String, Integer> getLivingList() {
        return this.livingList;
    }

    /**
     * runThroughList: creates the map of isAlive checks, going through every node in the list.
     * -1 signals the sender. the value of the map is the number of times the check has failed.
     *
     * @throws OutOfHeapSpaceException
     * @throws UnknownHostException
     */
    public void runThroughList() throws OutOfHeapSpaceException, UnknownHostException {
        String hostName;
        String destName;

        for (int i = 0; i < nodeList.length; i++) {
            int counter;

            hostName = serverAddress.getHostAddress();
            destName = nodeList[i];
            if (!(hostName.equals(destName))) {
                byte[] responseFromNode = aliveQuery(destName);

                if (responseFromNode[16] == CMD_INVALID.getId()) {
                    counter = livingList.get(destName);

                    if (counter == -1) {
                        counter = 0;
                        livingList.put(destName, counter);
                    }
                } else {
                    counter = livingList.get(destName);
                    if (counter == -1) {
                        counter = 1;
                        livingList.put(destName, counter);
                    } else {
                        counter++;
                        livingList.put(destName, counter);
                    }
                }
            }
        }
        for (String key : livingList.keySet()) {
            logger.log(Level.INFO, "Node: " + key + ", counter: " + livingList.get(key));
        }
    }


    /**
     * aliveQuery: handles the packet building, sending and receiving. Sending to each node and returning the byte response from node.
     *
     * @param node
     * @return
     */
    private byte[] aliveQuery(String node) {
        RequestPayload requestPayload = null;
        byte[] responsePay = null;
        requestPayload = new RequestPayload(logger);


        requestPayload.setCommand(COMMAND.IS_ALIVE);

        try {
            if (ServerApp.debug)
                logger.log(Level.INFO, "Sending Alive-Query to '" + node + "', port=" + serverKeySpace.getPort(node));
            Request isAliveRequest = new Request(logger);
            isAliveRequest.setDestinationAddress(InetAddress.getByName(node));
            isAliveRequest.setDestinationPort(serverKeySpace.getPort(node) + Constants.SERVER_INTERNAL_PORT_OFFSET);
            isAliveRequest.setPayload(requestPayload);

            Response isAliveResponse = aliveQueryForwarder.sendAndReceive(isAliveRequest, Constants.SERVER_RECEIVE_TIMEOUT, true);
            responsePayload = isAliveResponse.getPayload();

            responsePay = responsePayload.getPayloadBytes();


        } catch (IOException e) {
            logger.log(Level.INFO, "ERR: IO Exception: " + e.getMessage());
        } catch (RequestTimeoutException e) {
            logger.log(Level.INFO, "ERR: Request timeout exception: " + e.getMessage());
        } catch (PayloadIncompleteException e) {
            e.printStackTrace();
        } catch (PayloadEmptyException e) {
            e.printStackTrace();
        }

        return responsePay;


    }

    @Override
    public void run() {

        for (int i = 0; i < 5; i++) {
            try {
                Thread.sleep(1000);
                this.runThroughList();

            } catch (OutOfHeapSpaceException e) {
                logger.log(Level.INFO, "ERR: Send out of heap space exception: " + e.getMessage());
            } catch (UnknownHostException e) {
                logger.log(Level.INFO, "ERR: Send unknown host exception: " + e.getMessage());
            } catch (InterruptedException e) {
                logger.log(Level.INFO, "ERR: Send interrupted exception:  " + e.getMessage());
            }
        }
    }


}

