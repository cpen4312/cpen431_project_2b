package com.s73504152.CPEN431.A5;

/**
 * Created by Simon on 28-02-2016.
 */

import com.s73504152.CPEN431.A5.Helper.Constants;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConsistentHashing {

    private final Logger logger;

    /**
     * Key ring. Integer is key, String is node name
     */
    private static TreeMap<Integer, String> keySpace;
    private final ServerApp serverApp;

    /**
     * Holds all nodes
     */
    private String[] allNodes;

    /**
     * Holds all ports, index corresponds to allNodes
     */
    private int[] allPorts;

    //TODO: create server/node status object

    public ConsistentHashing(ServerApp serverApp, Logger logger) {
        this.serverApp = serverApp;
        keySpace = new TreeMap<>();
        this.logger = logger;
    }

    public String[] getAllNodes() {
        return allNodes;
    }

    /**
     * //TODO: Change type? Integer output is limiting the function to output 4 bytes
     * Using MD5 hash to generate a hash of key with length (bytes) = outputLength
     *
     * @param key the key to be hashed
     * @return the hash of the key (always positive). Output length is KEY_LENGTH_BYTES
     */
    public Integer hashFunction(byte[] key) {
        byte[] hash;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            if (md.getDigestLength() < Constants.KEY_LENGTH_BYTES) {
                System.out.println("Output length larger than hash length");
                return null;
            }
            md.update(key);
            hash = md.digest();
            ByteBuffer buf = ByteBuffer.allocate(Constants.KEY_LENGTH_BYTES);
            buf.put(hash, md.getDigestLength() - Constants.KEY_LENGTH_BYTES, Constants.KEY_LENGTH_BYTES);
            buf.position(0); // reset position of buffer before converting to int

            Integer hashInt = buf.getInt();
            if (hashInt < 0)
                hashInt = -hashInt;

            return hashInt;

        } catch (NoSuchAlgorithmException e) {
            System.out.println("MessageDigest: No such algorithm");
            return null;
        }
    }

    /**
     * Initializes allNodes and nodeList from a file
     *
     * @param filename the file of nodes, one per line: <hostname>:<port>
     */
    public synchronized void initNodes(String filename) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(filename));

        int lines = 0;
        String line1;
        while ((line1 = in.readLine()) != null) {
            if (line1.length() != 0)
                lines++;
        }

        allNodes = new String[lines];
        allPorts = new int[lines];

        in = new BufferedReader(new FileReader(filename));

        String line;
        int i = 0;
        while ((line = in.readLine()) != null) {

            if (line.length() != 0) {
                String hostname = line.split(":")[0];
                String port = line.split(":")[1];
                //StringTokenizer st = new StringTokenizer(line);
                //String hostname = st.nextToken();

                // add node to keySpace
                addNode(hostname);

                allNodes[i] = hostname;
                allPorts[i] = Integer.parseInt(port);
                i++;
            }
        }

        logger.log(Level.INFO, "Initial node list:");
        for (i = 0; i < allNodes.length; i++) {
            logger.log(Level.INFO, "[" + i + "] " + allNodes[i]);
        }

        in.close();
    }

    /**
     * Adds a node to the key space, with the predefined number of replicas
     * Each replica has the form: hostname#1, hostname#2,..., hostname#NUMBER_OF_VIRTUAL_NODES
     *
     * @param node
     */
    public synchronized void addNode(String node) {
        for (int i = 1; i < Constants.NUMBER_OF_VIRTUAL_NODES + 1; i++) {
            String virtualNode = (node + "#" + i);
            Integer hash = hashFunction(virtualNode.getBytes());
            keySpace.put(hash, virtualNode);
        }
    }

    /**
     * Removes a node by name
     *
     * @param node Complexity: O(log n)
     */
    public synchronized void removeNode(String node) {
        for (int i = 1; i < Constants.NUMBER_OF_VIRTUAL_NODES + 1; i++) {
            String virtualNode = (node + "#" + i);
            Integer hash = hashFunction(virtualNode.getBytes());
            keySpace.remove(hash);
        }
    }

    /**
     * Determines if a node is up
     *
     * @param node
     * @return true if alive, else false
     */
    public boolean isNodeAlive(String node) {
        if (keySpace.isEmpty())
            return false;
        int hash = hashFunction((node + "#" + 1).getBytes());
        return keySpace.containsKey(hash);
    }

    /**
     * @param key
     * @return node currently corresponding to the bucket this key belongs in
     * Complexity: O(log n)
     */
    public synchronized String getNode(byte[] key) { //String is node name
        if (keySpace.isEmpty())
            return null;
        int hash = hashFunction(key);
        // if we didn't hit a node key directly, find next
        if (!keySpace.containsKey(hash)) {
            Integer nextKey = keySpace.ceilingKey(hash);
            // if no next key exists, first must be the next
            if (nextKey == null) {
                hash = keySpace.firstKey();
            } else {
                hash = nextKey;
            }
        }
        String node = keySpace.get(hash);
        return node.split("#")[0];
    }

    /**
     * Get the responsible node for a given key
     *
     * @param key
     * @return String node
     *
     */
    public String getResponsibleNode(byte[] key) {
        String node = this.getNode(key);

        // TODO: check if this causes problems: if only one server is left in the keyspace, always return this one as the responsible node
        int numberOfNodesAlive = (int) Math.ceil(keySpace.size() / Constants.REPLICATION_FACTOR);
        if (numberOfNodesAlive == 1) {
            return node;
        }

        // if same node (virtual node), pick immediate successor
        if (node.equals(serverApp.address.toString())) {
            node = this.getSuccessors(key, false, false).get(0);
        }
        return node;
    }


    /**
     * @param node
     * @return port for a specific port
     */
    public int getPort(String node) {
        for (int i = 0; i < allNodes.length; i++) {
            if (allNodes[i].equals(node)) {
                return allPorts[i];
            }
        }
        return -1;
    }

    public String getNodeFromPort(int externalServerPort) {
        for (int i = 0; i < allPorts.length; i++) {
            if (allPorts[i] == (externalServerPort)) {
                return allNodes[i];
            }
        }
        return null;
    }

    /**
     * @return total number of nodes
     */
    public int totalNodeCount() {
        return allNodes.length;
    }

    /**
     * Return a list of all nodes and their alive status
     *
     * @return
     */
    public String[] getServerStatusList() {
        String[] list = allNodes.clone();
        for (int i = 0; i < allNodes.length; i++) {
            if (!isNodeAlive(allNodes[i])) {
                list[i] = allNodes[i] + ": DOWN";
            }
        }
        return list;
    }

    public String[] getKeySpaceAsStringArray() {
        String[] space = new String[Constants.NUMBER_OF_VIRTUAL_NODES * totalNodeCount()];
        int i = 0;
        for (Map.Entry<Integer, String> entry : keySpace.entrySet()) {
            for (int j = 1; j <= Constants.NUMBER_OF_VIRTUAL_NODES; j++) {
                space[i] = entry.getKey() + ": " + entry.getValue();
            }
            i++;
        }
        return space;
    }

    /**
     * Return the successors of a key in the key space
     * The successors are the unique nodes appearing after the responsible node (clockwise)
     * @param key
     * @return
     */
    public LinkedList<String> getSuccessors(byte[] key, boolean keyEqualsADeadNode, boolean nodeAsVirtual) {
        LinkedList<String> successors = new LinkedList<>();

        String responsibleNode = getNode(key);

        if (keyEqualsADeadNode){
            responsibleNode = "dead";
        }

        Integer hash = hashFunction(key);

        int count = Constants.REPLICATION_FACTOR - 1;

        boolean repeat;

        while(count > 0){
            repeat = false;

            // next entry
            Map.Entry<Integer, String> entry = keySpace.higherEntry(hash);

            // if end of keySpace, wrap around
            if (entry == null) {
                entry = keySpace.firstEntry();
            }

            String entryNodeName = entry.getValue().split("#")[0];

            //update hash
            hash = entry.getKey();

            // if already found successor matches, skip
            for (String successor : successors) {
                if (entry.getValue().equals(successor)) {
                    repeat = true;
                    break;
                }
            }
            if (repeat) continue;

            // if responsible node matches, skip
            if (entryNodeName.equals(responsibleNode)) {
                continue;
            } else {
                // add new unique successor
                String node;
                if (nodeAsVirtual) {
                    node = entry.getValue();
                } else {
                    node = entryNodeName;
                }
                successors.add(node);
            }
            count--;
        }

        return successors;
    }

    /**
     * Return the predecessors of a key in the key space
     * The predecessors are the unique nodes appearing after the responsible node (clockwise)
     * @param key
     * @param nodeAsVirtual
     * @return
     */
    public LinkedList<String> getPredecessors(byte[] key, boolean keyEqualsADeadNode, boolean nodeAsVirtual) {
        LinkedList<String> predecessors = new LinkedList<String>();

        String responsibleNode = getNode(key);

        if (keyEqualsADeadNode){
            responsibleNode = "dead"; // can't be equal to any living node name
        }

        Integer hash = hashFunction(key);

        int count = Constants.REPLICATION_FACTOR - 1;

        boolean repeat;

        while(count > 0){
            repeat = false;

            // next entry
            Map.Entry<Integer, String> entry = keySpace.lowerEntry(hash);

            // if end of keySpace, wrap around
            if (entry == null) {
                entry = keySpace.lastEntry();
            }

            String entryNodeName = entry.getValue().split("#")[0];

            //update hash
            hash = entry.getKey();

            // if already found predecessor matches, skip
            for (String predecessor : predecessors) {
                if (entryNodeName.equals(predecessor)) repeat = true; break;
            }
            if (repeat) continue;

            // if responsible node matches, skip
            if (entryNodeName.equals(responsibleNode)) {
                continue;
            } else {
                // add new unique predecessor
                String node;
                if (nodeAsVirtual) {
                    node = entry.getValue();
                } else {
                    node = entryNodeName;
                }
                predecessors.add(node);
            }
            count--;
        }

        return predecessors;
    }

}
