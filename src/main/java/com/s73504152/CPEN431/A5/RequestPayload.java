package com.s73504152.CPEN431.A5;


import com.s73504152.CPEN431.A5.Exceptions.*;
import com.s73504152.CPEN431.A5.Helper.ByteArrayHelper;
import com.s73504152.CPEN431.A5.Helper.ByteOrder;
import com.s73504152.CPEN431.A5.Helper.COMMAND;
import com.s73504152.CPEN431.A5.Helper.Constants;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.*;

/**
 * Created by chbndrhnns on 27.01.2016.
 */
/**
 * Class that represents a RequestPayload.
 */
public class RequestPayload {
    private Logger logger;

    public String getCmdByteAsHex() {
        return ByteArrayHelper.byteArrayToHex(cmdByte);
    }

    private byte[] cmdByte = null;
    private COMMAND command = null;
    private byte[] key = null;
    private byte[] value = null;
    private short valueLength = -1;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        // sb.append(ByteArrayHelper.byteArrayToString(messageId));
        if (command != null)
            if (command.getId() > 0) {
                sb.append(command.getId());
            }
        if (null != getKeyAsString()) {
            sb.append(ByteArrayHelper.byteArrayToString(getKey()));
        }
        if (valueLength != 0) {
            sb.append(valueLength);
            String val = getValueAsString();
            sb.append(val.substring(0, val.length()));
        }
        return sb.toString();
    }

    public COMMAND getCommand() throws PayloadCommandMissingException {
        if (this.command == null) {
            throw new PayloadCommandMissingException();
        }
        return this.command;
    }

    public void setCommand(COMMAND command) {
        this.command = command;
    }

    /*
    sets a key
     */
    public void setKey(byte[] key) throws KeyLenMismatchException {
        if (null == this.key) this.key = new byte[Constants.KEY_LEN];
        if (key.length > Constants.KEY_LEN) throw new KeyLenMismatchException();
        // byte[] paddedKey = new byte[Constants.KEY_LEN];
        System.arraycopy(key, 0, this.key, 0, key.length);
        // this.key = paddedKey;
    }

    /*
    gets a key as String
     */
    public String getKeyAsString() {
        if (this.key == null) return null;
        return javax.xml.bind.DatatypeConverter.printHexBinary(this.key);
    }

    /*
    gets a key as Byte array
     */
    public byte[] getKey() {
        return this.key;
    }

    /*
    gets a getValue as String
     */
    public String getValueAsString() {
        if (this.value == null) return "";
        return javax.xml.bind.DatatypeConverter.printHexBinary(this.value);
    }

    public short getValueLength() {
        return valueLength;
    }

    public byte[] getValue() {
        return this.value;
    }

    /*
    Sets a getValue and its length. Verifies the length.
     */
    public void setValue(byte[] value) throws PayloadValueTooLongException {
        this.value = value;
        this.valueLength = (short) value.length;
        if (this.valueLength > Constants.VAL_MAX_LEN) throw new PayloadValueTooLongException();
    }

    /*
    constructor
     */
    public RequestPayload(Logger logger) {
        this.logger = logger;
    }

    /*
    Creates a byte array that contains the data stored in this object.
     */
    public byte[] getPayloadBytes() throws PayloadValueLengthException, PayloadValueException, IOException, PayloadIncompleteException, PayloadEmptyException {
        // throw exceptions in case there is something missing
        if (this.command == null) throw new PayloadIncompleteException("command missing");
        if (this.key == null && (
                this.command.equals(COMMAND.GET) ||
                        this.command.equals(COMMAND.PUT) ||
                        this.command.equals(COMMAND.PUT_REPLICA) ||
                        this.command.equals(COMMAND.REMOVE) ||
                        this.command.equals(COMMAND.REMOVE_REPLICA))
                ) throw new PayloadIncompleteException("key missing");
        if (this.value == null && (
                        this.command.equals(COMMAND.PUT) ||
                        this.command.equals(COMMAND.PUT_REPLICA))
                ) throw new PayloadIncompleteException("key missing");
        // TODO: case that a key is set and the command is shutdown or delete_all has been deleted because it was not correctly handled
        if (this.command.equals(COMMAND.PUT) && this.valueLength == -1)
            throw new PayloadValueLengthException();
        if (this.command.equals(COMMAND.PUT) && this.value == null) throw new PayloadValueException();

        byte[] payload = null;
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            byte[] cmd;
            byte[] val_len;

            cmd = ByteArrayHelper.intToByteArray(this.command.getId());
            outputStream.write(cmd);
            if (this.command.equals(COMMAND.PUT) || this.command.equals(COMMAND.PUT_REPLICA)) {
                val_len = ByteArrayHelper.shortToByteArray(this.valueLength);
                outputStream.write(this.key);
                outputStream.write(val_len);
                outputStream.write(this.value);
            } else if (this.command.equals(COMMAND.GET)) {
                outputStream.write(this.key);
            } else if (this.command.equals(COMMAND.REMOVE) || this.command.equals(COMMAND.REMOVE_REPLICA)) {
                outputStream.write(this.key);
            }
            payload = outputStream.toByteArray();
        } catch (OutOfMemoryError | NullPointerException e) {
            e.printStackTrace();
        }

        if (payload == null) throw new PayloadEmptyException();
        return payload;
    }

    /*
    parses a given RequestPayload (mostly for test cases)
     */
    public void parseByteArray(byte[] payload, byte[] messageId) throws PayloadEmptyException, PayloadCommandInvalidException {

        if (payload.length == 0) {
            throw new PayloadEmptyException();
        }

        COMMAND cmd;
        byte[] valLen = new byte[Constants.VAL_LEN_LEN];
        // byte[] key;

        int offset = 0;

        // get command
        if (this.cmdByte == null) cmdByte = new byte[Constants.CMD_LEN];
        System.arraycopy(payload, 0, cmdByte, 0, Constants.CMD_LEN);
        cmd = COMMAND.valueOf(cmdByte[0]);
        if (cmd == null) {
            throw new PayloadCommandInvalidException("Could not parse command=" + cmd);
        }
        this.command = cmd;
        offset += Constants.CMD_LEN;

        StringBuilder sb = new StringBuilder();
        if (ServerApp.debug) {
            sb.append("[R] ");
            sb.append("msgId=").append(ByteArrayHelper.byteArrayToString(messageId));
            sb.append(", cmd=");
            sb.append(this.command.toString());
        }

        switch (cmd) {
            case PUT:
            case PUT_REPLICA:
                try {
                    // get key
                    if (null == this.key) this.key = new byte[Constants.KEY_LEN];
                    System.arraycopy(payload, offset, this.key, 0, Constants.KEY_LEN);
                    offset += Constants.KEY_LEN;

                    System.arraycopy(payload, offset, valLen, 0, Constants.VAL_LEN_LEN);
                    offset += Constants.VAL_LEN_LEN;
                    this.valueLength = (short) ByteOrder.leb2int(valLen, 0, 2);
                    if (this.valueLength < 1 || this.valueLength > 10000)
                        throw new PayloadValueLengthInvalidException();
                    if (this.valueLength > (payload.length - offset)) throw new PayloadValueLengthExceedsBuffer();

                    // get getValue
                    this.value = new byte[this.valueLength];
                    System.arraycopy(payload, offset, this.value, 0, this.valueLength);

                    // debug log
                    if (ServerApp.debug) {
                        sb.append(", key=");
                        sb.append(ByteArrayHelper.byteArrayToString(this.key));
                        sb.append(", len=");
                        sb.append(this.valueLength);
                        sb.append(", val=");
                        sb.append(ByteArrayHelper.byteArrayToString(this.value).substring(0, Math.min(this.valueLength, Constants.LOGGER_PRINT_MAX_VAL_LEN)));
                        sb.append("[...]");
                    }
                } catch (PayloadValueLengthInvalidException | PayloadValueLengthExceedsBuffer e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    logger.log(Level.INFO, "ERR: Something went wrong. Input data: '" +
                            ByteArrayHelper.byteArrayToString(payload).substring(0,
                                    Math.min(payload.length, Constants.LOGGER_PRINT_MAX_PAYLOAD_LEN)) + "'");
                }
                break;
            case GET:
            case REMOVE:
            case REMOVE_REPLICA:
                try {
                    if (null == this.key) this.key = new byte[Constants.KEY_LEN];
                    System.arraycopy(payload, offset, this.key, 0, Constants.KEY_LEN);

                    if (ServerApp.debug) {
                        sb.append(", key=");
                        sb.append(ByteArrayHelper.byteArrayToString(this.key));
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                break;
            case DELETE_ALL:
                if (ServerApp.debug) logger.log(Level.INFO, "Deleting the complete kv store.");
                break;
            case SHUTDOWN:
                logger.log(Level.INFO, "Shutting down.");
                break;
        }
        if (ServerApp.debug) {
            logger.log(Level.INFO, sb.toString());
        }
    }

//    /*
//    throws a PayloadValueLengthInvalidException if the getValue has in invalid length
//     */
//    private boolean valueLengthIsValid(int bytesRemaining) throws PayloadValueLengthInvalidException {
//        boolean valid = true;
//        //logger.log(Level.INFO, this.valueLength + " - " + bytesRemaining);
//        if (this.valueLength < 1 ||
//                this.valueLength > 10000 ||
//                this.valueLength > bytesRemaining) {
//            valid = false;
//        }
//        return valid;
//    }

}
